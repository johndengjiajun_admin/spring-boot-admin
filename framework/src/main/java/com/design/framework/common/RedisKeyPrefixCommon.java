package com.design.framework.common;

/**
 * Redis key 前缀常量
 * @author JohnDeng
 * @dateTime 2019年5月30日上午11:33:27
 */
public class RedisKeyPrefixCommon {

	/**
	 * 系统用户TOKEN
	 */
	public final static String TOKEN_NAME="token:system:";
	
	/**
	 * 登录超时
	 */
	public final static String LOGIN_COUNT="login:count:";
	
}
