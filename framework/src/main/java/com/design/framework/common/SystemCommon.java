package com.design.framework.common;

/**
 * 系统公共
 * @author JohnDeng
 * @dateTime 2020年2月3日下午7:17:08	
 *
 */
public class SystemCommon {

	/**
	 * 主数据源
	 */
	public static final String MASTER ="master";
	
	/**
	 * 登录过期时间，单位秒，默认8小时
	 */
	public static final Long LOGIN_SESSION_TIME_VALUE = 8*60*60L;
}
