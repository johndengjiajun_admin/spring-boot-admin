package com.design.framework.cache;

import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
/**
 * redis缓存
 * @author JohnDeng
 * @dateTime 2020年6月20日下午11:37:05	
 *
 */
@Component
public class RedisTemplateCache implements BaseCache {

	@Resource
	private RedisTemplate<String, Object> redisTemplate ;
	
	
	@Override
	public boolean set(String key, Object value) {
		redisTemplate.boundValueOps(key).set(value);
		return exists(key);
	}

	@Override
	public boolean set(String key, Object value, long expire) {
		redisTemplate.boundValueOps(key).set(value, expire, TimeUnit.SECONDS);
		return exists(key);
	}

	@Override
	public boolean exists(String key) {
		return redisTemplate.hasKey(key);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T get(String key, Class<T> clazz) {
		if (exists(key)) {
			return (T) redisTemplate.boundValueOps(key).get();
		} else {
			return null;
		}
	}

}
