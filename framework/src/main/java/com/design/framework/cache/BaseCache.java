package com.design.framework.cache;

/**
 * 缓存接口
 * @author JohnDeng
 * @dateTime 2020年6月20日下午11:41:16	
 *
 */
public interface BaseCache {

	/**
	 * 保存
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年12月22日上午6:30:24
	 * @param key
	 * @param value
	 * @return
	 */
	default boolean set(String key, Object value) {
		return false;
	}

	default boolean set(String key, Object value, long expire) {
		return false;
	}

	/**
	 * key是否存在
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年12月22日上午6:30:20
	 * @param key
	 * @return
	 */
	default boolean exists(String key) {
		return false;
	}

	default boolean delete(String key) {
		return false;
	}

	default <T> T get(String key, Class<T> clazz) {

		return null;
	}

}
