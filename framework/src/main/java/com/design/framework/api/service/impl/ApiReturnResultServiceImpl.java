package com.design.framework.api.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.design.framework.api.bean.ApiData;
import com.design.framework.api.bean.ApiError;
import com.design.framework.api.bean.ApiMap;
import com.design.framework.api.bean.ApiPage;
import com.design.framework.api.enums.SystemApiCodeEnum;
import com.design.framework.api.service.ApiReturnResultService;


/**
 * API返回实现类
 * @auth JohnDeng
 * @dateTime 2020年6月20日下午11:53:44	
 *
 */
@Service
public class ApiReturnResultServiceImpl implements ApiReturnResultService {
	
	private Map<String, Object> appendMap = null;

	@Override
	public void addMap(String key, Object value) {
		if (appendMap == null) {
			appendMap = new HashMap<>();
		}
		appendMap.put(key, value);
	}
	
	@Override
	public ApiData returnSucccssMap() {
		ApiData apiData = new ApiData();
		apiData.setData(appendMap);
		apiData.setCode(SystemApiCodeEnum.SUCCESS.getCode());
		apiData.setMsg(SystemApiCodeEnum.SUCCESS.getMessage());
		appendMap=null;
		return apiData;
	}

	@Override
	public ApiMap returnSuccess() {
		ApiMap apiCodeMsg = new ApiMap();
		apiCodeMsg.setCode(SystemApiCodeEnum.SUCCESS.getCode());
		apiCodeMsg.setMsg(SystemApiCodeEnum.SUCCESS.getMessage());
		return apiCodeMsg;
	}

	@Override
	public ApiMap returnParamError() {
		ApiMap apiCodeMsg = new ApiMap();
		apiCodeMsg.setCode(SystemApiCodeEnum.PARAM_ERROR.getCode());
		apiCodeMsg.setMsg(SystemApiCodeEnum.PARAM_ERROR.getMessage());
		return apiCodeMsg;

	}

	@Override
	public ApiMap returnInputIsempty() {
		ApiMap apiCodeMsg = new ApiMap();
		apiCodeMsg.setCode(SystemApiCodeEnum.INPUT_IS_NULL.getCode());
		apiCodeMsg.setMsg(SystemApiCodeEnum.INPUT_IS_NULL.getMessage());
		return apiCodeMsg;
	}

	@Override
	public ApiMap returnCustomCodeMsg(String code, String msg) {
		ApiMap apiCodeMsg = new ApiMap();
		apiCodeMsg.setCode(code);
		apiCodeMsg.setMsg(msg);
		return apiCodeMsg;
	}

	@Override
	public ApiMap returnBusinessError() {
		ApiMap apiCodeMsg = new ApiMap();
		apiCodeMsg.setCode(SystemApiCodeEnum.BUSINESS_ERROR.getCode());
		apiCodeMsg.setMsg(SystemApiCodeEnum.BUSINESS_ERROR.getMessage());
		return apiCodeMsg;
	}

	@Override
	public ApiMap returnPage(ApiPage apiPage) {
		ApiData apiData = new ApiData();
		apiData.setCode(SystemApiCodeEnum.SUCCESS.getCode());
		apiData.setMsg(SystemApiCodeEnum.SUCCESS.getMessage());
		apiData.setData(apiPage);
		return apiData;
	}

	@Override
	public ApiMap returnTokenError() {
		ApiMap apiCodeMsg = new ApiMap();
		apiCodeMsg.setCode(SystemApiCodeEnum.TOKEN_ERROR.getCode());
		apiCodeMsg.setMsg(SystemApiCodeEnum.TOKEN_ERROR.getMessage());
		return apiCodeMsg;
	}

	@Override
	public ApiMap returnDataSuccess(Object object) {
		ApiData apiData = new ApiData();
		apiData.setCode(SystemApiCodeEnum.SUCCESS.getCode());
		apiData.setMsg(SystemApiCodeEnum.SUCCESS.getMessage());
		apiData.setData(object);
		return apiData;
	}

	@Override
	public ApiMap returnParamError(String msg) {
		ApiMap apiCodeMsg = new ApiMap();
		apiCodeMsg.setCode(SystemApiCodeEnum.CUSTOM_PARAM_ERROR.getCode());
		apiCodeMsg.setMsg(msg);
		return apiCodeMsg;
	}

	@Override
	public ApiMap returnBusinessError(String exception) {
		ApiError error=new ApiError();
		error.setCode(SystemApiCodeEnum.BUSINESS_ERROR.getCode());
		error.setMsg(SystemApiCodeEnum.BUSINESS_ERROR.getMessage());
		error.setError(exception);
		return error;
	}

	@Override
	public ApiMap returnTokenIsNull() {
		ApiMap apiCodeMsg = new ApiMap();
		apiCodeMsg.setCode(SystemApiCodeEnum.TOKEN_IS_NULL.getCode());
		apiCodeMsg.setMsg(SystemApiCodeEnum.TOKEN_IS_NULL.getMessage());
		return apiCodeMsg;
	}

	@Override
	public ApiMap returnCustomCodeMsg(String code, String msg, Object value) {
		ApiData apiData = new ApiData();
		apiData.setCode(code);
		apiData.setMsg(msg);
		apiData.setData(value);
		return apiData;
	}


}
