package com.design.framework.api.bean;

/**
 * api 处理异常信息类
 * @author JohnDeng
 * 2018年12月15日上午1:56:35 
 *
 */
public class ApiError extends ApiMap{

	/**
	 * @author JohnDeng
	 * 2018年12月15日上午1:56:24  
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 异常信息
	 */
	private  String error;

	/**
	 * @author JohnDeng
	 * 2018年12月15日上午1:57:52 
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * @author JohnDeng
	 * 2018年12月15日上午1:57:52 
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}
}
