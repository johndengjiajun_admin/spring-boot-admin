package com.design.framework.api.enums;

/**
 * API状态码和返回信息
 *
 * @author JohnDeng
 */
public enum SystemApiCodeEnum {

  /**
   * 操作成功
   */
  SUCCESS("0000", "操作成功"),
  /**
   * 参数异常
   */
  PARAM_ERROR("0001", "参数异常"),
  /**
   * Id不能为空
   */
  ID_IS_NULL("0002", "Id不能为空"),
  /**
   * 输入数据为空
   */
  INPUT_IS_NULL("0003", "输入数据为空"),
  /**
   * 业务异常
   */
  BUSINESS_ERROR("0004", "业务异常"),
  /**
   * Id不存在
   */
  ID_IS_ERROR("0006", "Id不存在"),
  /**
   * 自定义参数异常信息
   */
  CUSTOM_PARAM_ERROR("0007", "自定义参数异常信息"),
  /**
   * 无法连接Redis
   */
  CONNOT_GET_REDIS_CONNECTION("9994", "无法连接Redis"),
  /**
   * URL没有权限
   */
  URL_NOT_PERMISSION("9995", "URL没有权限"),
  /**
   * Token不存在
   */
  TOKEN_IS_NONENTITY("9996", "Token不存在"),
  /**
   * Token为空
   */
  TOKEN_IS_NULL("9997", "Token为空"),
  /**
   * Token异常
   */
  TOKEN_ERROR("9999", "Token异常"),
  /**
   * 请求参数有误或者语义有误
   */
  ERROR_400("400", "请求参数有误或者语义有误"),
  /**
   * 请求体不能为空
   */
  ERROR_401("401", "请求体不能为空"),
  /**
   * 请求路径异常
   */
  ERROR_404("404", "请求路径异常"),
  /**
   * 请求方法不正确
   */
  ERROR_405("405", "请求方法不正确"),
  /**
   * 请求媒体不接受
   */
  ERROR_406("406", "请求媒体不接受"),
  /**
   * 请求资源格式不支持
   */
  ERROR_415("415", "请求资源格式不支持"),
  /**
   * 服务器异常
   */
  ERROR_500("500", "服务器异常"),
  /**
   * 请求接口超时
   */
  ERROR_408("408", "请求接口超时"),
  /**
   * 上传文件不存在
   */
  FILE_IS_NULL("2000", "上传文件不存在"),
  /**
   * 文件类型错误
   */
  FILE_TYPE_ERROR("2001", "文件类型错误"),
  /**
   * 文件上传过最大文件大小
   */
  FILE_OVER_MAX_SIZE("2002", "文件上传过最大文件大小");

  private final String code;
  private final String message;

  SystemApiCodeEnum(String code, String message) {
    this.code = code;
    this.message = message;
  }

  public String getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }
}
