package com.design.framework.api.service;

import com.design.framework.api.bean.ApiData;
import com.design.framework.api.bean.ApiMap;
import com.design.framework.api.bean.ApiPage;

/**
 * API返回接口
 * @author JohnDeng
 * @dateTime 2020年6月20日下午11:38:50	
 *
 */
public interface ApiReturnResultService {

	/**
	 * 返回成功的状态码和信息
	 * 
	 * @author JohnDeng
	 * @date 2018年8月1日下午2:07:40
	 * @return
	 */
	ApiMap returnSuccess();

	
	/**
	 * 返回带有数据成功的状态码和信息
	 * @author JohnDeng
	 * @date 2018年8月1日下午4:53:34
	 * @return
	 */
	ApiMap returnDataSuccess(Object object);
	
	/**
	 * 返回参数异常的状态码和信息
	 * 
	 * @author JohnDeng
	 * @date 2018年8月1日下午2:07:14
	 * @return
	 */
	ApiMap returnParamError();
	
	/**
	 * 自定义参数异常信息
	 * @author JohnDeng
	 * @date 2018年8月1日下午5:11:42
	 * @param msg
	 * @return
	 */
	ApiMap returnParamError(String msg);

	/**
	 * 返回输入为空的状态码和信息
	 * 
	 * @author JohnDeng
	 * @date 2018年8月1日下午2:06:34
	 * @return
	 */
	ApiMap returnInputIsempty();

	/**
	 * 自定义返回状态码和信息
	 * 
	 * @author JohnDeng
	 * @date 2018年8月1日下午2:05:06
	 * @return
	 */
	ApiMap returnCustomCodeMsg(String code, String msg);
	
	/**
	 * 自定义返回状态码和信息和值
	 * @author JohnDeng
	 * @datetime 2019年11月6日下午4:20:27
	 * @param code
	 * @param msg
	 * @param value
	 * @return
	 */
	ApiMap returnCustomCodeMsg(String code, String msg,Object value);

	/**
	 * 返回业务异常状态码和信息
	 * 
	 * @author JohnDeng
	 * @date 2018年8月1日下午2:04:44
	 * @return
	 */
	ApiMap returnBusinessError();
	
	/**
	 * 返回业务异常状态码和信息
	 * @param exception
	 * @return
	 * @author JohnDeng
	 * 2018年12月15日下午12:28:49
	 */
	ApiMap returnBusinessError(String exception);

	/**
	 * 返回分页
	 * 
	 * @author JohnDeng
	 * @date 2018年8月1日下午2:04:22
	 * @param apiPage
	 * @return
	 */
	ApiMap returnPage(ApiPage apiPage);
	
	
	/**
	 *  返回token异常
	 * @author JohnDeng
	 * @date 2018年8月1日下午4:48:16
	 * @return
	 */
	ApiMap returnTokenError();
	
	
	/**
	 *  返回Token为空
	 * @author JohnDeng
	 * @dateTime 2019年1月25日下午2:30:57
	 * @return
	 */
	ApiMap returnTokenIsNull();
	
	
	/**
	 *  添加键值对
	 * @param key
	 * @param value
	 * @author JohnDeng
	 * 2018年9月24日上午10:23:14
	 */
	void addMap(String key, Object value);
	
	/**
	 *  返回一个Map
	 * @return
	 * @author JohnDeng
	 * 2018年9月24日上午10:24:07
	 */
	ApiData returnSucccssMap();
}
