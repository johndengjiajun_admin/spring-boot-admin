package com.design.framework.api;

import javax.annotation.Resource;

import com.design.framework.api.bean.ApiMap;
import com.design.framework.api.bean.ApiPage;
import com.design.framework.api.service.ApiReturnResultService;
import com.design.framework.api.service.impl.ApiReturnResultServiceImpl;
import com.design.framework.bean.Page;
import com.design.framework.bean.QueryPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 返回前端的实体
 * 
 * @author JohnDeng
 * @dateTime 2018年7月6日下午6:57:02
 */

public class ApiResponse {


	private ApiReturnResultService apiReturnResultService=new ApiReturnResultServiceImpl();

	/**
	 * 返回一个成功map
	 * 
	 * @return
	 */
	public ApiMap returnSucccssMap() {

		return apiReturnResultService.returnSucccssMap();
	}

	/**
	 * 新增属性到data下面
	 * 
	 * @param key
	 * @param value
	 */

	public void addMap(String key, Object value) {

		apiReturnResultService.addMap(key, value);
	}

	/**
	 * 返回成功的状态码和信息
	 * 
	 * @author JohnDeng
	 * @date 2018年8月1日下午2:07:40
	 * @return
	 */
	public ApiMap returnSuccess() {
		return apiReturnResultService.returnSuccess();
	}

	/**
	 * 返回带有数据成功的状态码和信息
	 * 
	 * @author JohnDeng
	 * @date 2018年8月1日下午4:53:34
	 * @return
	 */
	public ApiMap returnDataSuccess(Object object) {

		return apiReturnResultService.returnDataSuccess(object);
	}

	/**
	 * 返回参数异常的状态码和信息
	 * 
	 * @author JohnDeng
	 * @date 2018年8月1日下午2:07:14
	 * @return
	 */
	public ApiMap returnParamError() {

		return apiReturnResultService.returnParamError();
	}

	/**
	 * 返回输入为空的状态码和信息
	 * 
	 * @author JohnDeng
	 * @date 2018年8月1日下午2:06:34
	 * @return
	 */
	public ApiMap returnInputIsempty() {

		return apiReturnResultService.returnInputIsempty();
	}

	/**
	 * 自定义返回状态码和信息
	 * 
	 * @author JohnDeng
	 * @date 2018年8月1日下午2:05:06
	 * @return
	 */
	public ApiMap returnCustomCodeMsg(String code, String msg) {

		return apiReturnResultService.returnCustomCodeMsg(code, msg);
	}

	/**
	 * 自定义返回状态码和信息和值
	 * 
	 * @author JohnDeng
	 * @date 2018年8月1日下午2:05:06
	 * @return
	 */
	public ApiMap returnCustomCodeMsgValue(String code, String msg, Object value) {

		return apiReturnResultService.returnCustomCodeMsg(code, msg, value);
	}

	/**
	 * 返回业务异常状态码和信息
	 * 
	 * @author JohnDeng
	 * @date 2018年8月1日下午2:04:44
	 * @return
	 */
	public ApiMap returnBusinessError() {

		return apiReturnResultService.returnBusinessError();
	}

	public ApiMap returnBusinessError(String exception) {

		return apiReturnResultService.returnBusinessError(exception);
	}

	/**
	 * 返回分页
	 * 
	 * @author JohnDeng
	 * @date 2018年8月1日下午2:04:22
	 * @param apiPage
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public ApiMap returnPage(Page page, QueryPage paging) {

		return apiReturnResultService.returnPage(
				new ApiPage(page.getTotalRecord(), paging.getCurrPage(), paging.getPageSize(), page.getResults()));
	}

	/**
	 * 返回token异常
	 * 
	 * @author JohnDeng
	 * @date 2018年8月1日下午4:48:16
	 * @return
	 */
	public ApiMap returnTokenError() {

		return apiReturnResultService.returnTokenError();
	}

	/**
	 * 返回token为空
	 * 
	 * @author JohnDeng
	 * @date 2018年8月1日下午4:48:16
	 * @return
	 */
	public ApiMap returnTokenIsNull() {

		return apiReturnResultService.returnTokenIsNull();
	}

	/**
	 * 自定义参数异常信息
	 * 
	 * @author JohnDeng
	 * @date 2018年8月1日下午5:12:06
	 * @param msg
	 * @return
	 */
	public ApiMap returnParamError(String msg) {

		return apiReturnResultService.returnParamError(msg);
	}
}
