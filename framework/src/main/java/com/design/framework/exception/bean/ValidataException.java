package com.design.framework.exception.bean;

/**
 * 检验异常类
 * @author JohnDeng
 * @date 2018年8月14日下午4:51:18
 */
public class ValidataException extends CustomException {


	private static final long serialVersionUID = 1L;

	public ValidataException(String msg) {
		  super(msg);
		  this.msg=msg;
	}

}
