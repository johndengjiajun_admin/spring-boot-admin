package com.design.framework.exception.bean;
/**
 * 自定义异常
 * @author JohnDeng
 * @dateTime 2018年7月5日下午4:32:46
 */
public class CustomException  extends Exception{

	protected String code;
	
	protected String msg;

	/**
	 * @author JohnDeng
	 * @dateTime 2018年7月5日下午4:36:20
	 */
	private static final long serialVersionUID = 1L;

	
	public CustomException(String message){
		super(message);
	}
	
	public CustomException(String code,String msg){
		super(msg);
		this.code=code;
		this.msg=msg;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
