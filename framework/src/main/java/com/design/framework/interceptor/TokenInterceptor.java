package com.design.framework.interceptor;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.design.framework.api.enums.SystemApiCodeEnum;
import com.design.framework.api.service.ApiReturnResultService;
import com.design.framework.cache.RedisTemplateCache;
import com.design.framework.common.RedisKeyPrefixCommon;
import com.design.framework.common.SystemCommon;
import com.design.framework.utils.MemoryUtils;
import com.design.framework.utils.SpringContextHolderUtils;

/**
 * Token拦截器
 * 
 * @author JohnDeng
 * @date 2018年8月8日下午3:36:49
 */
public class TokenInterceptor implements HandlerInterceptor {

	public Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private RedisTemplateCache redis=SpringContextHolderUtils.getBean(RedisTemplateCache.class);

	@Resource
	private ApiReturnResultService apiReturnResultServiceService;
	

	public Long getLoginSessionTime() {

		return SystemCommon.LOGIN_SESSION_TIME_VALUE;
	}

	public List<String> getTokenFilteUrlConfig() {

		return null;
	}


	/**
	 * preHandle方法是进行处理器拦截用的，顾名思义，该方法将在Controller处理之前进行调用，
	 * SpringMVC中的Interceptor拦截器是链式的，可以同时存在
	 * 多个Interceptor，然后SpringMVC会根据声明的前后顺序一个接一个的执行， 而且所有的Interceptor中的preHandle方法都会在
	 * Controller方法调用之前调用。SpringMVC的这种Interceptor链式结构也是可以进行中断的， 这种中断方式是令preHandle的返
	 * 回值为false，当preHandle的返回值为false的时候整个请求就结束了。
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		logger.info("\n===================tokenInterceptor start===================");

		if (CollectionUtils.isNotEmpty(getTokenFilteUrlConfig())) {
			for (String url : getTokenFilteUrlConfig()) {
				if (request.getRequestURI().contains(url)) {
					logger.info("\n===================tokenInterceptor pass===================");
					return true;
				}
			}
		}

		String token = request.getHeader("Token");
		// 检验token
		if (StringUtils.isNotEmpty(token)) {
			// 是否有这个token
			boolean ishasToken = false;
			try {
				ishasToken = redis.exists(RedisKeyPrefixCommon.TOKEN_NAME + token);
			} catch (NullPointerException e) {
				tokenIsNonentity(response);
			}
			// 有token
			if (ishasToken) {
				String tokenValue =  redis.get(RedisKeyPrefixCommon.TOKEN_NAME + token,String.class);
				if (tokenValue == null) {
					isEmptyToken(response);
				} else {
					redis.set(RedisKeyPrefixCommon.TOKEN_NAME + token, tokenValue, getLoginSessionTime());
					logger.info("\n===================tokenInterceptor pass===================");
					return true;
				}
			} else {
				tokenIsNonentity(response);
			}
		}
		return false;
	}

	/**
	 * 这个方法只会在当前这个Interceptor的preHandle方法返回值为true的时候才会执行。postHandle是进行处理器拦截用的，
	 * 它的执行时间是在处理器进行处理之 后，也就是在Controller的方法调用之后执行，但是它会在DispatcherServlet进行视图的渲染之前执行，
	 * 也就是说在这个方法中你可以对ModelAndView进行操
	 * 作。这个方法的链式结构跟正常访问的方向是相反的，也就是说先声明的Interceptor拦截器该方法反而会后调用，
	 * 这跟Struts2里面的拦截器的执行过程有点像，
	 * 只是Struts2里面的intercept方法中要手动的调用ActionInvocation的invoke方法，
	 * Struts2中调用ActionInvocation的invoke方法就是调用下一个Interceptor
	 * 或者是调用action，然后要在Interceptor之前调用的内容都写在调用invoke之前，
	 * 要在Interceptor之后调用的内容都写在调用invoke方法之后。
	 */
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}

	
	/**
	 * 该方法也是需要当前对应的Interceptor的preHandle方法的返回值为true时才会执行。该方法将在整个请求完成之后，
	 * 也就是DispatcherServlet渲染了视图执行，
	 * 这个方法的主要作用是用于清理资源的，当然这个方法也只能在当前这个Interceptor的preHandle方法的返回值为true时才会执行。
	 */
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// 打印JVM信息
		if (logger.isInfoEnabled()) {
			logger.info("===================" + MemoryUtils.getMemory() + "===================");
		}
	}

	
	
	/**
	 * token为空
	 *
	 * @param httpResponse
	 * @throws IOException
	 * @author John
	 * @dateTime 2017年11月27日下午3:18:59
	 */
	private void isEmptyToken(HttpServletResponse httpResponse) throws IOException {
		PrintWriter out = httpResponse.getWriter();
		out.println(JSONObject.toJSON(apiReturnResultServiceService.returnTokenIsNull()));
		return;
	}

	/**
	 * Token不存在
	 * @author JohnDeng
	 * @dateTime 2020年1月28日上午11:52:05	
	 * @param httpResponse
	 * @throws IOException
	 */
	private void tokenIsNonentity(HttpServletResponse httpResponse) throws IOException {
		PrintWriter out = httpResponse.getWriter();
		out.println(JSONObject.toJSON(apiReturnResultServiceService.returnCustomCodeMsg(SystemApiCodeEnum.TOKEN_IS_NONENTITY.getCode(),
				SystemApiCodeEnum.TOKEN_IS_NONENTITY.getMessage())));
		return;
	}

	/**
	 * URL没有权限
	 * @author JohnDeng
	 * @dateTime 2020年1月28日上午11:51:00	
	 * @param httpResponse
	 * @throws IOException
	 */
	private void urlNotPermission(HttpServletResponse httpResponse) throws IOException {
		PrintWriter out = httpResponse.getWriter();
		out.println(JSONObject.toJSON(apiReturnResultServiceService.returnCustomCodeMsg(SystemApiCodeEnum.URL_NOT_PERMISSION.getCode(),
				SystemApiCodeEnum.URL_NOT_PERMISSION.getMessage())));
		return ;
	}

}
