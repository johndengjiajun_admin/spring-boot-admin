package com.design.framework.tree;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.design.framework.utils.MyBeanUtils;

/**
 * 树形接口控制器
 * 
 * @author JohnDeng
 * @date 2018年8月14日上午11:34:33
 * @param <T>
 * @param <ID>
 */
public class TreeServiceImpl<T, ID> implements TreeService<T, ID> {

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getChildrenList(String idKey,String parentIdKey ,ID parentId, T entity, List<T> dateList) {

		List<T> nowList = null;
		if (CollectionUtils.isNotEmpty(dateList)) {
			nowList = new ArrayList<T>();
			for (T t : dateList) {
				ID id = (ID) MyBeanUtils.getProperty(t, parentIdKey);
				if (id.equals(parentId)) {
					nowList.add(t);
				}
			}
		}
		return nowList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TreeNode<ID>> getTree(String idKey,String parentIdKey ,ID parentIdValue, T entity, List<T> dateList) {
		List<T> menuList = getChildrenList(idKey,parentIdKey,parentIdValue, entity, dateList);
		List<TreeNode<ID>> nodeList = new ArrayList<TreeNode<ID>>();
		TreeNode<ID> node = null;
		if (CollectionUtils.isNotEmpty(menuList)) {
			for (T tt : menuList) {
				node = new TreeNode<ID>();
				ID id = (ID) MyBeanUtils.getProperty(tt, idKey);
				node = entityToTreeNode(node, tt);
				node.setChildren(getTree(idKey,parentIdKey,id, entity, dateList));
				nodeList.add(node);
			}
		}
		return nodeList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public TreeNode<ID> entityToTreeNode(TreeNode<ID> node, T entity) {
		node.setId((ID) MyBeanUtils.getProperty(entity, "id"));
		node.setName((String) MyBeanUtils.getProperty(entity, "name"));
		node.setParentId((ID) MyBeanUtils.getProperty(entity, "parentId"));
		return node;
	}

}
