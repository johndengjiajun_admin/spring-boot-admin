package com.design.framework.tree;

import java.util.List;

/**
 *   树
 * @author JohnDeng
 * @date 2018年8月14日上午11:49:00
 * @param <T>
 */
public interface TreeService<T,ID> {

	/**
	 * 获取子列表
	 * 
	 * @param idKey  id字段名
	 * @param parentIdKey  父id的字段名
	 * @param parentIdValue  父id值
	 * @param entity 实体
	 * @param dateList 获取的数据
	 * @return
	 * @
	 * @author John
	 * @dateTime 2017年11月23日上午11:20:06
	 */
	public List<T> getChildrenList(String idKey,String parentIdKey ,ID parentId, T entity, List<T> dateList) ;

	/**
	 * 获取树
	 * @param idKey  id字段名
	 * @param parentIdKey  父id的字段名
	 * @param parentIdValue  父id值
	 * @param entity 实体
	 * @param dateList 获取的数据
	 * @return
	 * @
	 * @author John
	 * @dateTime 2017年11月23日上午11:20:24
	 */
	public List<TreeNode<ID>> getTree(String idKey,String parentIdKey ,ID parentIdValue, T entity, List<T> dateList) ;

	/**
	 * 实体转换树节点
	 * 
	 * @param entity
	 * @return
	 * @author John
	 * @dateTime 2017年11月23日上午11:20:37
	 */
	public TreeNode<ID> entityToTreeNode(TreeNode<ID> node,T entity);
}
