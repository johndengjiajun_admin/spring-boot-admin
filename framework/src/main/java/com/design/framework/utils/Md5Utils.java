package com.design.framework.utils;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * MD5工具类
 * 
 * @author JohnDeng
 * @date 2018年8月8日下午3:39:28
 */
public class Md5Utils {


	/**
	 * md5
	 * @param context
	 * @return
	 */
	public static String encrypByMd5(String context) {
		return DigestUtils.md5Hex(context);
	}

	
}
