package com.design.framework.utils;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.design.framework.cache.RedisTemplateCache;
import com.design.framework.common.RedisKeyPrefixCommon;
/**
 * token工具类
 * @author JohnDeng
 * 2017年11月10日下午5:40:35
 */
public class TokenUtils {

	
	private static final String KEY="token";
	
	/**
	 * 生产token
	 * UUID+盐经过MD5加密
	 * @author JohnDeng
	 * 2017年11月10日下午5:40:48
	 * @return
	 */
	public static String getToken(){
		
		return Md5Utils.encrypByMd5(IdUtils.getId()+KEY+DateUtil.getMillis(new Date()));
	}
	
	/**
	 * 获取token
	 * 
	 * @author johnDeng
	 * @dateTime 2019年3月19日上午10:05:10
	 * @param request
	 * @return
	 */
	public static String getToken(HttpServletRequest request) {
		return request.getHeader("Token");
	}
	
	public static String getUserId(HttpServletRequest request) {
		String token = getToken(request);
		if (StringUtils.isEmpty(token)) {
			return null;
		}
		try {
			
			RedisTemplateCache redis=SpringContextHolderUtils.getBean(RedisTemplateCache.class);
			
			if (redis.exists(RedisKeyPrefixCommon.TOKEN_NAME+token)) {
			
				return redis.get(RedisKeyPrefixCommon.TOKEN_NAME+token, String.class);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
