package com.design.framework.utils;

/**
 * 对象工具
 * @author JohnDeng
 * @dateTime 2020年6月20日下午11:42:42	
 *
 * @param <T>
 */
public class Tool<T> {

	private T object;

	public T getObject() {
		return object;
	}

	public void setObject(T object) {
		this.object = object;
	}
	
	public Tool() {
		
	}


	public Tool(T object) {
		this.object = object;
	}

	
}
