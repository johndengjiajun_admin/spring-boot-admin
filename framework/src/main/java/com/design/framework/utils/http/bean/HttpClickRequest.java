package com.design.framework.utils.http.bean;

import java.util.Map;

/**
 * HTTP请求对象
 * 
 * @author JohnDeng
 * @dateTime 2019年5月15日上午11:19:15
 */
public class HttpClickRequest {
	/**
	 * URL
	 */
	private String url;

	/**
	 * 请求头
	 */
	private Map<String, String> headers;

	/**
	 * 请求参数
	 */
	private Map<String, String> params;

	/**
	 * 是否设置string传输
	 */
	private boolean stringEntityFlag;

	/**
	 * json to String
	 */
	private String jsonString;

	public HttpClickRequest() {

	};

	public HttpClickRequest(String url, Map<String, String> headers, Map<String, String> params,
			boolean stringEntityFlag, String jsonString) {
		super();
		this.url = url;
		this.headers = headers;
		this.params = params;
		this.stringEntityFlag = stringEntityFlag;
		this.jsonString = jsonString;
	}

	public HttpClickRequest(Builder builder) {
		this.url = builder.url;
		this.headers = builder.headers;
		this.params = builder.params;
		this.stringEntityFlag = builder.stringEntityFlag;
		this.jsonString = builder.jsonString;
	}

	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	public Map<String, String> getParams() {
		return params;
	}

	public void setParams(Map<String, String> params) {
		this.params = params;
	}

	public boolean isStringEntityFlag() {
		return stringEntityFlag;
	}

	public void setStringEntityFlag(boolean stringEntityFlag) {
		this.stringEntityFlag = stringEntityFlag;
	}

	public String getJsonString() {
		return jsonString;
	}

	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {
		private String url;
		private Map<String, String> headers;
		private Map<String, String> params;
		private boolean stringEntityFlag;
		private String jsonString;
		

		public HttpClickRequest build() {
	        return new HttpClickRequest(this);
	    }

		public Builder url( String url) {
			this.url = url;
			return this;
		}

		public Builder headers( Map<String, String> headers) {
			this.headers = headers;
			return this;
		}

		public Builder params( Map<String, String> params) {
			this.params = params;
			return this;
		}

		public Builder stringEntityFlag(boolean  stringEntityFlag) {
			this.stringEntityFlag = stringEntityFlag;
			return this;
		}

		public Builder jsonString( String jsonString) {
			this.jsonString = jsonString;
			return this;
		}

	}

}
