package com.design.framework.utils;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;

/**
 * RSA算法
 * @author JohnDeng
 * @datetime 2019年7月9日下午2:10:32
 *
 */
public class RSACryptography {

	private final static String RSA="RSA";
	
	/**
	 * 生成密钥对
	 * @author JohnDeng
	 * @datetime 2019年7月9日下午2:10:44
	 * @param keyLength
	 * @return
	 * @throws Exception
	 */
	public static KeyPair genKeyPair() throws Exception {
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		keyPairGenerator.initialize(1024);
		return keyPairGenerator.generateKeyPair();
	}

	/**
	 * 公钥加密
	 * @author JohnDeng
	 * @datetime 2019年7月9日下午2:14:22
	 * @param content
	 * @param publicKey
	 * @return
	 * @throws Exception
	 */
	public static byte[] encrypt(byte[] content, PublicKey publicKey) throws Exception {
		Cipher cipher = Cipher.getInstance(RSA);
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		return cipher.doFinal(content);
	}

	/**
	 * 私钥解密
	 * @author JohnDeng
	 * @datetime 2019年7月9日下午2:14:26
	 * @param content
	 * @param privateKey
	 * @return
	 * @throws Exception
	 */
	public static byte[] decrypt(byte[] content, PrivateKey privateKey) throws Exception {
		Cipher cipher = Cipher.getInstance(RSA);
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		return cipher.doFinal(content);
	}

	/**
	 * 将base64编码后的公钥字符串转成PublicKey
	 * @author JohnDeng
	 * @datetime 2019年7月9日下午2:14:31
	 * @param publicKey
	 * @return
	 * @throws Exception
	 */
	public static PublicKey getPublicKey(String publicKey) throws Exception {
		byte[] keyBytes = Base64.getDecoder().decode(publicKey.getBytes());
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(RSA);
		return keyFactory.generatePublic(keySpec);
	}

	/**
	 * 将base64编码后的私钥字符串转成PrivateKey
	 * @author JohnDeng
	 * @datetime 2019年7月9日下午2:14:37
	 * @param privateKey
	 * @return
	 * @throws Exception
	 */
	public static PrivateKey getPrivateKey(String privateKey) throws Exception {
		byte[] keyBytes = Base64.getDecoder().decode(privateKey.getBytes());
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(RSA);
		return keyFactory.generatePrivate(keySpec);
	}
	
	/**
	 * 加密转base64字符串
	 * @author JohnDeng
	 * @datetime 2019年7月9日下午4:41:27
	 * @param content
	 * @param publicKey
	 * @return
	 * @throws Exception
	 */
	public static String encryptBase64String(byte[] content, PublicKey publicKey) throws Exception{
		byte[] encryptByte=encrypt(content, publicKey);
		return new String(Base64.getEncoder().encode(encryptByte));
	}
	
	
	/**
	 * 解密Base64字符串
	 * @author JohnDeng
	 * @datetime 2019年7月9日下午4:46:56
	 * @param base64encrypted
	 * @param privateKey
	 * @return
	 * @throws Exception
	 */
	public static String decryptBase64String(String base64encrypted,PrivateKey privateKey ) throws Exception{
		byte[] keyBytes = Base64.getDecoder().decode(base64encrypted.getBytes());
		byte[] decrypt= decrypt(keyBytes, privateKey);
		return new String(decrypt);
	}
	

}
