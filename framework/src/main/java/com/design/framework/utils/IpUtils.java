package com.design.framework.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

/**
 * IP工具类
 * 
 * @author Join
 *
 */
public class IpUtils {

	private  static  final  String UNKNOWN="unknown";

	/**
	 * 获取本地IP
	 * 
	 * @param request
	 * @return
	 */
	public static String getLocalIp(HttpServletRequest request) {
		return request.getLocalAddr();
	}

	/**
	 * 获取反向代理服务器地址
	 * @param request
	 * @return
	 */
	public static String getIpAddr(HttpServletRequest request) { 
	       String ip = request.getHeader("x-forwarded-for"); 
	       if(ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
	           ip = request.getHeader("Proxy-Client-IP"); 
	       } 
	       if(ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
	           ip = request.getHeader("WL-Proxy-Client-IP"); 
	       } 
	       if(ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
	           ip = request.getRemoteAddr(); 
	       } 
	       return ip; 
	   }
	
}
