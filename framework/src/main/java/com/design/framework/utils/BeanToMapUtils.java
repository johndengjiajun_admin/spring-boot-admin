package com.design.framework.utils;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
/**
 * 实体转MAP工具类
 * @author JohnDeng
 * @dateTime 2020年6月20日下午11:35:29	
 *
 */
public class BeanToMapUtils {

  /**
   * 实体转MAP
   *
   * @param bean
   * @return
   * @throws IntrospectionException
   * @throws InvocationTargetException
   * @throws IllegalAccessException
   * @author JohnDeng
   * @dateTime 2019年5月28日上午9:57:52
   */
  public static Map<String, Object> beanToMap(Object bean) throws Exception {
    // 获取对象字节码信息,不要Object的属性
    BeanInfo beanInfo = Introspector.getBeanInfo(bean.getClass(), Object.class);
    // 获取bean对象中的所有属性
    PropertyDescriptor[] list = beanInfo.getPropertyDescriptors();
    // 创建Map集合对象
    Map<String, Object> map = new HashMap<String, Object>(list.length);

    for (PropertyDescriptor pd : list) {
      // 获取属性名
      String key = pd.getName();
      // 调用getter()方法,获取内容
      String value = (pd.getReadMethod().invoke(bean) == null) ? null : String.valueOf(pd.getReadMethod().invoke(bean));
      // 增加到map集合当中
      map.put(key, value);
    }
    return map;
  }

  public static Map<String, String> beanToMapString(Object bean) throws Exception {

    // 获取对象字节码信息,不要Object的属性
    BeanInfo beanInfo = Introspector.getBeanInfo(bean.getClass(), Object.class);
    // 获取bean对象中的所有属性
    PropertyDescriptor[] list = beanInfo.getPropertyDescriptors();
    // 创建Map集合对象
    Map<String, String> map = new HashMap<String, String>(list.length);

    for (PropertyDescriptor pd : list) {
      // 获取属性名
      String key = pd.getName();
      // 调用getter()方法,获取内容
      String value = pd.getReadMethod().invoke(bean) == null ? null : pd.getReadMethod().invoke(bean).toString();
      // 增加到map集合当中
      map.put(key, value);
    }
    return map;
  }


}
