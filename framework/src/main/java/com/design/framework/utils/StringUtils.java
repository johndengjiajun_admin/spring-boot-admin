package com.design.framework.utils;

/**
 * 字符串工具类
 * @author JohnDeng
 * @dateTime 2020年6月20日下午11:41:57	
 *
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils {

	
	public static boolean isEmpty(Object... obj) {
		if (obj.length > 0) {
			for (Object o : obj) {
				if (o == null || "".equals(o)) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean isNotEmpty(Object... obj) {

		return !isEmpty(obj);
	}
	
	public static void main(String[] args) {
	
		StringUtils.isNotEmpty("id","name","xxx");
	}
}
