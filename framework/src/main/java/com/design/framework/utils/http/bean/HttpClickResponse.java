package com.design.framework.utils.http.bean;

import java.util.Map;

/**
 * HTTP请求返回对象
 * 
 * @author JohnDeng
 * @dateTime 2019年5月15日上午11:21:43
 */
public class HttpClickResponse {

	private int statusCode;

	private Map<String, String> responseHeadersMap;

	private String bodyParams;

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public Map<String, String> getResponseHeadersMap() {
		return responseHeadersMap;
	}

	public void setResponseHeadersMap(Map<String, String> responseHeadersMap) {
		this.responseHeadersMap = responseHeadersMap;
	}

	public String getBodyParams() {
		return bodyParams;
	}

	public void setBodyParams(String bodyParams) {
		this.bodyParams = bodyParams;
	}

}
