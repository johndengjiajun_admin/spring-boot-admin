package com.design.framework.annotion;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.design.framework.common.SystemCommon;

/**
 * 数据库分离配置 dataSourceRead:读取数据 dataSourceWrite：写入数据
 * 
 * @author JohnDeng
 * @dateTime 2018年3月26日下午4:11:08
 *
 */
@Target({ ElementType.PARAMETER, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited()
public @interface DataSource {

	/**
	 * 数据源，默认是主数据源
	 * @author JohnDeng
	 * @dateime 2019年3月31日下午10:53:41
	 * @return String
	 */
	String value() default SystemCommon.MASTER;

}
