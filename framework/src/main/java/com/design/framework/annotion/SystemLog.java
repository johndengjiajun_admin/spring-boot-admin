package com.design.framework.annotion;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.design.framework.enums.SystemLogOperateEnums;
import com.design.framework.enums.SystemLogTypeEnums;

/**
 * 系统日志注解
 * 
 * @author John
 * @datetime 2017年12月1日上午10:16:14
 */
@Target({ ElementType.PARAMETER, ElementType.METHOD ,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited()
public @interface SystemLog {

	/**
	 * 操作类型，比如查询，新增，修改，删除，登录，退出，导入，导出
	 * 
	 * @return SystemLogOperateEnums
	 * @author John
	 * @dateTime 2017年12月1日上午10:15:55
	 */
	SystemLogOperateEnums operate() default SystemLogOperateEnums.SELECT;

	/**
	 * 日志描述 默认是空字符
	 * 
	 * @return String
	 * @author John
	 * @dateTime 2017年12月1日上午10:16:05
	 */
	String description() default "";

	/**
	 * 媒体类型，默认是PC端
	 *
	 * @return SystemLogTypeEnums
	 * @author JohnDeng 2018年12月15日上午12:10:37
	 */
	SystemLogTypeEnums type() default SystemLogTypeEnums.PC;
	
	/**
	 * 模块名称 比如：用户管理
	 * @return String
	 */
	String modelName() default "";

}
