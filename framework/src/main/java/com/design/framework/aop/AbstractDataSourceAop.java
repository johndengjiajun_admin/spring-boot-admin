package com.design.framework.aop;

import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

import com.design.framework.annotion.DataSource;
import com.design.framework.common.SystemCommon;

/**
 * 面向切面切换数据库
 * 
 * @author JohnDeng
 * @dateTime 2018年3月26日下午4:12:21
 *
 */

public  abstract class AbstractDataSourceAop {
	
	@Pointcut("@annotation(com.design.framework.annotion.DataSource)")
	public void dataSourceBefore() {

	}

	/**
	 * 根据数据源id直接匹配名称，若没有使用注解默认调用read数据
	 * 
	 * @author JohnDeng
	 * @date 2018年8月14日下午2:54:32
	 * @param jp
	 */
	@Before("dataSourceBefore()")
	public void before(JoinPoint jp) {
		doDataSource(jp);
	}
	
	
	/**
	 * 配置数据库类型
	 * @author JohnDeng
	 * @dateTime 2019年5月30日上午11:36:04
	 */
	public abstract void doDataSource(JoinPoint jp);
	

	/**
	 * 获取DataSource注解的type
	 * 
	 * @param joinPoint
	 * @return String
	 */
	public String getMthodTableType(JoinPoint joinPoint) {
		String targetName = joinPoint.getTarget().getClass().getName();
		String methodName = joinPoint.getSignature().getName();
		Object[] arguments = joinPoint.getArgs();
		Class<?> targetClass;
		//默认是主数据源
		String value = SystemCommon.MASTER;
		try {
			targetClass = Class.forName(targetName);
			Method[] methods = targetClass.getMethods();
			for (Method method : methods) {
				if (method.getName().equals(methodName)) {
					Class<?>[] clazzs = method.getParameterTypes();
					if (clazzs.length == arguments.length) {
						value = method.getAnnotation(DataSource.class).value();
						break;
					}
				}
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return value;
	}

}
