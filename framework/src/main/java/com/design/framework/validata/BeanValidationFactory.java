package com.design.framework.validata;
/**
 * bean检验工厂
 * @author JohnDeng
 * @dateTime 2020年6月20日下午11:36:02	
 *
 */
public class BeanValidationFactory {
	
	public static <T>BeanValidation<T> getBean(T entity) {
		
		return new BeanValidation<T>();
	}
}
