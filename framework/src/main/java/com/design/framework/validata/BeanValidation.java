package com.design.framework.validata;

import java.lang.reflect.Field;
import java.util.List;

import javax.persistence.Id;

import com.design.framework.utils.ReflectUtils;
import com.design.framework.validata.bean.ValidationResult;

/**
 * 实体检验类
 * 
 * @author JohnDeng
 * @date 2018年8月8日下午3:41:34
 */
public class BeanValidation<T> {

	/**
	 * 参数检验，检验失败return
	 * 
	 * @author JohnDeng
	 * @dateTime 2018年9月12日下午8:04:29
	 * @param object
	 * @param validationResult
	 * @return
	 */
	public  boolean validateParamFail(T t, ValidationResult validationResult,Class<?> ... groups) {
		ValidatorBeanFactory<T> validator =new ValidatorBeanFactory<T>();
		return validator.validataBean(t, validationResult, groups);
	}

	/**
	 * 检验ID true没值,false有值
	 * 
	 * @author JohnDeng 2017年11月9日上午11:57:21
	 * @param clazz
	 * @return
	 */
	public static boolean validIdFail(Object object) {
		if (object == null) {
			return false;
		}
		List<Field> fields = ReflectUtils.getFields(ReflectUtils.getClass(object));
		for (Field field : fields) {
			Id id = field.getAnnotation(Id.class);
			if (id != null) {
				try {
					Object value = ReflectUtils.getMethodValue(object, field);
					if (value == null || "".equals(value)) {
						return true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return false;
	}

}
