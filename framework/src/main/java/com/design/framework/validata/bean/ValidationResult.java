package com.design.framework.validata.bean;

import javax.validation.Path;

/**
 * 检验返回的结果
 * @author JohnDeng
 * @date 2018年8月8日下午3:42:18
 */
public class ValidationResult {
	

	/**
	 * 字段名
	 */
	private String fieldName;
	
	/**
	 * 获取 验证错误 信息 message
	 */
	private String message;
	
	/**
	 * 获取错误的字段名称
	 */
	private Path path;
	
	public Path getPath() {
		return path;
	}

	public void setPath(Path path) {
		this.path = path;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
}
