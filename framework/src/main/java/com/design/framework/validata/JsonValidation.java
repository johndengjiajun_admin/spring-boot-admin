package com.design.framework.validata;

import com.alibaba.fastjson.JSONObject;
import com.design.framework.validata.bean.ValidationResult;

/**
 * JSON检验
 * @author JohnDeng
 * @date 2018年8月8日下午3:41:20
 */
public class JsonValidation {

	/**
	 * JSON是否含有指定参数,没有指定的全部参数返回false,空字符串也返回false
	 * 有指定全部的参数返回true
	 * @author JohnDeng
	 * 2017年11月10日下午5:25:23
	 * @param jsonObject
	 * @param paramName
	 * @return
	 */
	public static boolean containsKeys(ValidationResult result,JSONObject jsonObject,Object ... paramName){
		if(jsonObject==null || paramName==null ){
			return false;
		}
		try {
			for(Object key: paramName){
				if(!jsonObject.containsKey(key)){
					result.setMessage("欠缺"+key);
					return false;
				}else {
					if(jsonObject.get(key)==null|| "".equals(jsonObject.get(key))){
						result.setMessage(key+"不能为空");
						return false;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	/**
	 * 没包含的指定的参数返回true
	 * @param result
	 * @param jsonObject
	 * @param paramName
	 * @return
	 *@author JohnDeng
	 *@time 2018年7月20日下午2:11:18
	 */
	public static boolean  notContainsKeys(ValidationResult result,JSONObject jsonObject,Object ... paramName){
		
		return !containsKeys(result, jsonObject, paramName);
	}
}
