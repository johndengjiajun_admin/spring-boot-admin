package com.design.framework.datasource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 动态配置数据库
 * 
 * @author JohnDeng
 * @dateTime 2018年3月26日下午4:13:19
 *
 */
public class DynamicDataSourceHolder {

	private static Logger logger = LoggerFactory.getLogger(DynamicDataSourceHolder.class);

	/**
	 * 注意：数据源标识保存在线程变量中，避免多线程操作数据源时互相干扰
	 */
	private static final ThreadLocal<String> THREAD_DATA_SOURCE = new ThreadLocal<String>();

	public static void setDataSource(String dataSource) {
		logger.info("切换到{" + dataSource + "}数据源");
		THREAD_DATA_SOURCE.set(dataSource);
	}

	public static void remove() {
		THREAD_DATA_SOURCE.remove();
	}

	public static String getDataSource() {
		return THREAD_DATA_SOURCE.get();
	}
}
