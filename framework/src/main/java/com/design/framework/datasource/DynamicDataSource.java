package com.design.framework.datasource;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 多数据源
 * @author JohnDeng
 * @dateTime 2020年6月20日下午11:39:43	
 *
 */
public class DynamicDataSource extends AbstractRoutingDataSource {

	@Override
	protected Object determineCurrentLookupKey() {
		String name=DynamicDataSourceHolder.getDataSource();
		DynamicDataSourceHolder.remove();
		return name;
	}
}
