package com.design.framework.bean;

import java.util.List;

/**
 * Token没有不过滤的请求路径列表
 * @author JohnDeng
 * @dateTime 2018年7月6日下午6:55:17
 */
public class PathMatchingFilter {
	
	private List<String> path;

	public List<String> getPath() {
		return path;
	}

	public void setPath(List<String> path) {
		this.path = path;
	}
	
	
}
