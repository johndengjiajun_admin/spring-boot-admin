package com.design.framework.base.controller;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.design.framework.annotion.SystemLog;
import com.design.framework.api.bean.ApiMap;
import com.design.framework.base.entity.BaseEntity;
import com.design.framework.base.service.BaseService;
import com.design.framework.bean.Page;
import com.design.framework.bean.QueryPage;
import com.design.framework.enums.SystemLogOperateEnums;
import com.design.framework.exception.bean.CustomException;
import com.design.framework.utils.JsonUtils;

/**
 * 基类控制器
 *
 * @param <T>
 * @author JohnDeng 2017年11月9日上午10:32:42
 */
public abstract class BaseController<T extends BaseEntity<ID>, ID> extends ValidateBaseController<T, ID> {

	public Logger log =LoggerFactory.getLogger(getClass());
	
	public BaseService<T, ID> baseService;

	protected Class<T> entityClass;

	protected Class<ID> id;

	@Autowired
	protected HttpServletRequest request;
	@Autowired
	protected HttpServletResponse response;

	public abstract void initialize();

	/**
	 * 通过构造函数获取泛型
	 * 
	 * @author JohnDeng 2018年9月24日上午10:29:45
	 */
	@SuppressWarnings("unchecked")
	public BaseController() {
		this.entityClass = null;
		Class<?> c = getClass();
		Type type = c.getGenericSuperclass();
		if (type instanceof ParameterizedType) {
			Type[] parameterizedType = ((ParameterizedType) type).getActualTypeArguments();
			this.entityClass = (Class<T>) parameterizedType[0];
			this.id = (Class<ID>) parameterizedType[1];
		}
	}

	public void setBaseService(BaseService<T, ID> baseService) {
		this.baseService = baseService;
	}

	/**
	 * 新增之前操作的方法（可以做业务逻辑）
	 * 
	 * @author JohnDeng
	 * @dateTime 2018年9月12日下午8:58:42
	 * @param entity
	 * @param json
	 */
	protected void beforeAdd(T entity, JSONObject json)throws CustomException {
		log.info("beforeAdd:{}",json);
	}

	protected void beforeUpdate(T entity, JSONObject json) throws CustomException{
		log.info("beforeUpdate:{}",json);
	}
	
	protected void beforeDelete(T entity, ID id) throws CustomException{
		log.info("beforeDelete:{}",id);
	}

	protected void afterGetListByPage(Page<T> page) throws CustomException{
		
	}

	protected void beforeGetListByPage(JSONObject json, T entity,Page<T> page) throws CustomException{
		log.info("beforeGetListByPage:{}",json);
	}

	protected void beforeGetList() throws CustomException{

	}

	protected void beforeGetListMap(String key, String value) throws CustomException{
	}

	protected void afterGetList(List<T> entityList)throws CustomException {

	}

	protected void afterGetListMap(List<Map<String, Object>> listMap) throws CustomException{

	}

	/**
	 * 新增
	 * 
	 * @author JohnDeng
	 * @param entity
	 * @param json
	 * @return 
	 * @throws Exception
	 */
	@SystemLog(operate = SystemLogOperateEnums.INSERT, description = "新增")
	@ResponseBody
	@PostMapping("/add")
	public ApiMap add(T entity, @RequestBody JSONObject json) throws Exception {
		entity = JsonUtils.toBean(json, entity);
		validateAdd(json, entity);
		beforeAdd(entity, json);
		entity.setAddParam();
		baseService.insert(entity);
		return returnSuccess();
	}

	/**
	 * 根据id删除一个对象
	 *
	 * @param entity
	 * @return
	 * @throws Exception
	 * @author JohnDeng 2017年11月9日下午3:28:45
	 */
	@SystemLog(operate = SystemLogOperateEnums.DELETE, description = "根据id删除一个对象")
	@ResponseBody
	@DeleteMapping("/delete/{id}")
	public ApiMap delete(T entity, @PathVariable("id") ID id) throws Exception {
		validateDelete(id, entity);
		beforeDelete(entity,id);
		baseService.delete(entity);
		return returnSuccess();
	}

	/**
	 * 批量删除
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年3月21日下午4:04:39
	 * @param entity
	 * @param json
	 * @return
	 * @throws Exception
	 */
	@SystemLog(operate = SystemLogOperateEnums.DELETE, description = "批量删除")
	@ResponseBody
	@DeleteMapping("/batchDelete")
	public ApiMap batchDelete(T entity, @RequestBody JSONObject json) throws Exception {
		System.out.println(json);
		List<T> list = JsonUtils.toBeanList(json.getJSONArray("ids"), entity);
		baseService.deleteBatchById(list);
		return returnSuccess();
	}

	/**
	 * 修改
	 * 
	 * @author JohnDeng
	 * @param entity
	 * @param json
	 * @return
	 * @throws Exception
	 */
	@SystemLog(operate = SystemLogOperateEnums.UPDATE, description = "修改")
	@ResponseBody
	@PostMapping("/update")
	public ApiMap update(T entity, @RequestBody JSONObject json) throws Exception {
		entity = JsonUtils.toBean(json, entity);
		validateUpdate(json, entity);
		beforeUpdate(entity, json);
		entity.setUpdateParam();
		baseService.update(entity);
		return returnSuccess();
	}

	/**
	 * 根据ID获取实体
	 * 
	 * @param entity
	 * @param id
	 * @return
	 * @author JohnDeng
	 * @throws Exception
	 */
	@SystemLog(operate = SystemLogOperateEnums.SELECT, description = "根据ID获取实体")
	@ResponseBody
	@GetMapping("/get/{id}")
	public ApiMap get(T entity, @PathVariable("id") ID id) throws Exception {
		validateGet(id, entity);
		entity = baseService.findById(id);
		return returnDataSuccess(entity);
	}

	/**
	 * 查询全部列表
	 * 
	 * @return
	 * @author JohnDeng
	 * @throws Exception
	 */
	@SystemLog(operate = SystemLogOperateEnums.SELECT, description = "查询全部列表")
	@ResponseBody
	@GetMapping("/getList")
	public ApiMap getList() throws Exception {
		beforeGetList();
		List<T> entityList = baseService.findAll();
		afterGetList(entityList);
		return returnDataSuccess(entityList);
	}

	/**
	 * 根据key value 获取listMap
	 * 
	 * @param map
	 * @param key
	 * @param value
	 * @return
	 * @throws Exception
	 */
	@SystemLog(operate = SystemLogOperateEnums.SELECT, description = "根据key value 获取listMap")
	@ResponseBody
	@GetMapping("/getListMap/{key}/{value}")
	public ApiMap getListMap(Map<String, Object> map, @PathVariable("key") String key,
			@PathVariable("value") String value) throws Exception {
		beforeGetListMap(key, value);
		map.put(key, value);
		List<Map<String, Object>> listMap = baseService.getListMap(map);
		afterGetListMap(listMap);
		return returnDataSuccess(listMap);
	}

	/**
	 * 分頁
	 * 
	 * @param entity
	 * @param json
	 * @param page
	 * @param paging
	 * @return
	 * @throws Exception
	 */
	@SystemLog(operate = SystemLogOperateEnums.SELECT, description = "分页查询")
	@ResponseBody
	@PostMapping("/getListByPage")
	public ApiMap getListByPage(T entity, @RequestBody JSONObject json, Page<T> page, QueryPage paging)
			throws Exception {
		paging = JsonUtils.toBean(json, paging);
		validateGetListByPage(json, paging, page);
		beforeGetListByPage(json, entity,page);
		page = baseService.getListByPage(page);
		afterGetListByPage(page);
		return returnPage(page, paging);
	}
}
