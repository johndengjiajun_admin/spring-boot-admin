package com.design.framework.base.controller;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.design.framework.api.ApiResponse;
import com.design.framework.api.enums.SystemApiCodeEnum;
import com.design.framework.bean.Page;
import com.design.framework.bean.QueryPage;
import com.design.framework.exception.bean.IdExcption;
import com.design.framework.exception.bean.ParameterException;
import com.design.framework.exception.bean.ValidataException;
import com.design.framework.utils.StringUtils;
import com.design.framework.validata.BeanValidation;
import com.design.framework.validata.BeanValidationFactory;
import com.design.framework.validata.bean.ValidationResult;
import com.design.framework.validata.group.Add;
import com.design.framework.validata.group.Update;
/**
 * 检验控制器
 * @author JohnDeng
 * @dateTime 2020年6月20日下午11:37:37	
 *
 * @param <T>
 * @param <ID>
 */
public class ValidateBaseController<T, ID> extends ApiResponse {

	
	/**
	 * 检验通用对象
	 * @param json
	 * @param object
	 * @throws ValidataException
	 */
	protected void validateObject(Object object) throws ValidataException {
		ValidationResult validation = new ValidationResult();
		if (BeanValidationFactory.getBean(object).validateParamFail(object, validation ,Add.class)) {
			throw new ValidataException(validation.getMessage());
		}
	}
	
	
	/**
	 * 检验新增方法参数
	 *
	 * @param json
	 * @param entity
	 * @return
	 * @throws Exception
	 * @author John
	 * @dateTime 2017年12月14日下午4:23:46
	 */
	protected void validateAdd(JSONObject json, T entity) throws ValidataException {
		ValidationResult validation = new ValidationResult();
		if (BeanValidationFactory.getBean(entity).validateParamFail(entity, validation ,Add.class)) {
			throw new ValidataException(validation.getMessage());
		}
	}

	/**
	 * 检验删除方法参数
	 *
	 * @param id
	 * @param entity
	 * @return
	 * @throws Exception
	 * @author John
	 * @dateTime 2017年12月14日下午4:22:10
	 */
	protected void validateDelete(ID id, T entity) throws IdExcption {
		if (StringUtils.isEmpty(id)) {
			throw new IdExcption(SystemApiCodeEnum.ID_IS_NULL.getMessage());
		}
	}

	/**
	 * 检验更新方法参数
	 *
	 * @param json
	 * @param entity
	 * @return
	 * @throws Exception
	 * @author John
	 * @dateTime 2017年12月14日下午4:33:09
	 */
	protected void validateUpdate(JSONObject json, T entity) throws ValidataException ,ParameterException {
		ValidationResult validationResult = new ValidationResult();
		if (BeanValidation.validIdFail(entity)) {
			throw new ParameterException(SystemApiCodeEnum.ID_IS_NULL.getMessage());
		} else if (BeanValidationFactory.getBean(entity).validateParamFail(entity, validationResult,Update.class)) {
			throw new ValidataException(validationResult.getMessage());
		}
	}

	/**
	 * 检验获取单个对象方法参数
	 *
	 * @param id
	 * @param entity
	 * @return
	 * @throws Exception
	 * @author John
	 * @dateTime 2017年12月14日下午4:37:50
	 */
	protected void validateGet(ID id, T entity) throws IdExcption {
		if (StringUtils.isEmpty(id)) {
			throw new IdExcption(SystemApiCodeEnum.ID_IS_NULL.getMessage());
		}
	}

	/**
	 * 检验getListMap入参
	 *
	 * @param key
	 * @param value
	 * @return
	 * @throws Exception
	 * @author John
	 * @dateTime 2017年12月22日上午11:03:35
	 */
	protected void validateGetListMap(Map<String, Object> map, String key, String value) throws ParameterException {
		if (StringUtils.isEmpty(key, value)) {
			throw new ParameterException(SystemApiCodeEnum.PARAM_ERROR.getMessage());
		}
	}

	/**
	 * 检验分页
	 *
	 * @param json
	 * @param paging
	 * @param page
	 * @throws Exception
	 * @author JohnDeng
	 * @time 2018年7月20日下午3:33:12
	 */
	protected void validateGetListByPage(JSONObject json, QueryPage paging, Page<T> page) throws ValidataException {
		ValidationResult validation = new ValidationResult();
		if (BeanValidationFactory.getBean(new QueryPage()).validateParamFail(paging, validation)) {
			throw new ValidataException(validation.getMessage());
		}
		if (paging.getCurrPage() > 0) {
			page.setPageNo(paging.getCurrPage());
		} else {
			paging.setCurrPage(1);
			page.setPageNo(paging.getCurrPage());
		}
		if (paging.getPageSize() > 0) {
			page.setPageSize(paging.getPageSize());
		} else {
			paging.setPageSize(1);
			page.setPageSize(paging.getPageSize());
		}
		if (StringUtils.isNotEmpty(paging.getOrderByName())) {
			page.setOrderByName(paging.getOrderByName());
		}
		if (StringUtils.isNotEmpty(paging.getOrderBy())) {
			page.setOrderBy(paging.getOrderBy());
		}
		if (paging.getParams() != null) {
			page.setParams(paging.getParams());
		}
	}

	/**
	 * 检验分页
	 *
	 * @param json
	 * @param paging
	 * @param page
	 * @throws Exception
	 * @author JohnDeng
	 * @time 2018年7月20日下午3:33:12
	 */
	protected void validateGetObjectListByPage(JSONObject json, QueryPage paging, Page<?> page) throws ValidataException {
		ValidationResult validation = new ValidationResult();
		if (BeanValidationFactory.getBean(new QueryPage()).validateParamFail(paging, validation)) {
			throw new ValidataException(validation.getMessage());
		}
		if (paging.getCurrPage() > 0) {
			page.setPageNo(paging.getCurrPage());
		} else {
			paging.setCurrPage(1);
			page.setPageNo(paging.getCurrPage());
		}
		if (paging.getPageSize() > 0) {
			page.setPageSize(paging.getPageSize());
		} else {
			paging.setPageSize(1);
			page.setPageSize(paging.getPageSize());
		}
		if (StringUtils.isNotEmpty(paging.getOrderByName())) {
			page.setOrderByName(paging.getOrderByName());
		}
		if (StringUtils.isNotEmpty(paging.getOrderBy())) {
			page.setOrderBy(paging.getOrderBy());
		}
		if (paging.getParams() != null) {
			page.setParams(paging.getParams());
		}
	}
}
