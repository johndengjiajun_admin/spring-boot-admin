package com.design.framework.base.service.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.design.framework.base.dao.BaseDao;
import com.design.framework.base.entity.BaseEntity;
import com.design.framework.base.service.BaseService;
import com.design.framework.bean.Page;
import com.design.framework.tree.TreeServiceImpl;

/**
 * 基类服务实现类
 * 
 * @author JohnDeng 2017年11月9日上午10:32:36
 * @param <T>
 */

@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class BaseServiceImpl<T extends BaseEntity<ID>, ID> extends TreeServiceImpl<T, ID>
		implements BaseService<T, ID> {

	public Logger log =LoggerFactory.getLogger(getClass());
	
	@Autowired
	public HttpServletRequest request;

	protected BaseDao<T, ID> baseDao;

	public void setBaseDao(BaseDao<T, ID> baseDao) {
		this.baseDao = baseDao;
	}

	@Override
	public T findById(ID id) {

		return baseDao.findById(id);
	}

	@Override
	public List<T> findAll(){

		return baseDao.findAll();
	}

	@Override
	public T get(T entity){

		return baseDao.get(entity);
	}

	@Override
	public List<T> getList(T entity){

		return baseDao.getList(entity);
	}

	@Override
	public Page<T> getListByPage(Page<T> page) throws Exception{
		page.setTotalRecord(baseDao.getTotalCount(page));
		List<T> list = baseDao.getListByPage(page);
		page.setResults(list);
		return page;
	}

	@Transactional(readOnly = false)
	@Override
	public int insert(T entity) throws Exception{

		return baseDao.insert(entity);
	}

	@Transactional(readOnly = false)
	@Override
	public int update(T entity)throws Exception {

		return baseDao.update(entity);
	}

	@Transactional(readOnly = false)
	@Override
	public int deleteById(ID id) throws Exception{

		return baseDao.deleteById(id);
	}

	@Transactional(readOnly = false)
	@Override
	public int deleteBatchById(List<T> list)throws Exception {

		return baseDao.deleteBatchById(list);
	}

	@Transactional(readOnly = false)
	@Override
	public int insertBatch(List<T> list)throws Exception {

		return baseDao.insertBatch(list);
	}

	@Transactional(readOnly = false)
	@Override
	public int updateBatchByIds(List<T> list) throws Exception{

		return baseDao.updateBatchById(list);
	}

	@Override
	public List<Map<String, Object>> getListMap(Map<String, Object> param) {

		return baseDao.getListMap(param);
	}

	@Override
	public List<Map<String, Object>> getListMap(T entity) {

		return baseDao.getListMap(entity);
	}

	@Override
	public int getTotalCount(Page<T> page) throws Exception{

		return baseDao.getTotalCount(page);
	}

	@Transactional(readOnly = false)
	@Override
	public void delete(T entity)throws Exception{

		baseDao.deleteById(entity.getId());
	}

}