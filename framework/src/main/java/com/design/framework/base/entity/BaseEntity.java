package com.design.framework.base.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.design.framework.utils.DateUtil;
import com.design.framework.utils.IdUtils;

/**
 * 实体基类
 * 
 * @author JohnDeng
 * @dateTime 2018年8月6日下午10:41:55
 */
public class BaseEntity<ID> implements Serializable {

	/**
	 * @author JohnDeng
	 * @dateTime 2018年8月6日下午10:04:36
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private ID id;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time")
    @DateTimeFormat(pattern=DateUtil.FORMAT_YMDHMS)
    @JSONField(name="createTime",format=DateUtil.FORMAT_YMDHMS)
	private Date createTime;

	/**
	 * 修改时间
	 */
	
	@Column(name = "update_time")
    @DateTimeFormat(pattern=DateUtil.FORMAT_YMDHMS)
    @JSONField(name="updateTime",format=DateUtil.FORMAT_YMDHMS)
	private Date updateTime;

	public ID getId() {
		return id;
	}

	public void setId(ID id) {
		this.id = id;
	}

	/**
	 * 新增统一设置新增ID，创建时间，修改时间，创建人ID，修改人ID,删除标志
	 * @author JohnDeng
	 * @date 2018年8月8日下午3:27:58
	 * @return
	 */
	public ID setAddParam() {
		ID id = (ID) IdUtils.getId();
		this.createTime = new Date();
		this.updateTime = new Date();
		this.id = id;
		return id;
	}

	public void setUpdateParam() {
		this.updateTime = new Date();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
	/**
	 * 对象转JSON字符串
	 * @author JohnDeng
	 * @dateTime 2019年12月6日下午9:55:12	
	 * @return
	 */
	public String toJsonString() {
		
		return JSON.toJSONString(this);
	}

}
