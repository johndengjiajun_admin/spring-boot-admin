package com.design.framework.enums;

/**
 * 日志媒体类型
 *
 * @author JohnDeng
 * @dateTime 2018年12月14日下午11:23:43
 */
public enum SystemLogTypeEnums implements IApiCodeMessage<String, String> {
  /**
   * 手机端
   */
  APP("APP", "手机端"),
  /**
   * 电脑端
   */
  PC("PC", "电脑端");

  private final String code;

  private final String message;

  SystemLogTypeEnums(String code, String message) {
    this.code = code;
    this.message = message;
  }

  @Override
  public String getCode() {
    return this.code;
  }

  @Override
  public String getMessage() {
    return this.message;
  }

  public static String getMessage(String code) {
    for (SystemLogTypeEnums e : SystemLogTypeEnums.values()) {
      if (e.getCode().equals(code)) {
        return e.getMessage();
      }
    }
    return null;
  }
}
