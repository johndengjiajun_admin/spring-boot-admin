package com.design.framework.enums;
/**
 * API枚举类获取Code和Message泛型
 * @author JohnDeng
 * @dateTime 2019年12月6日下午10:05:26	
 *
 * @param <CodeType>
 * @param <MassageType>
 */
public interface IApiCodeMessage<CodeType, MassageType> {
	
	 CodeType getCode();

	 MassageType getMessage();

}
