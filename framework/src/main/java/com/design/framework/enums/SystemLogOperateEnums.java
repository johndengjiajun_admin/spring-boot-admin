package com.design.framework.enums;

/**
 * 日志操作枚举
 * 
 * @author Administrator
 *
 */
public enum SystemLogOperateEnums implements IApiCodeMessage<String, String> {

	/**
	 * 查询
	 */
	SELECT("SELECT", "查询"),
	/**
	 * 新增
	 */
	INSERT("INSERT", "新增"),
	/**
	 * 修改
	 */
	UPDATE("UPDATE", "修改"),
	/**
	 * 删除
	 */
	DELETE("DELETE", "删除"),
	/**
	 * 登入
	 */
	LOGIN("LOGIN", "登入"),
	/**
	 * 登出
	 */
	LOGOUT("LOGOUT", "登出"),
	/**
	 * 导出
	 */
	EPORTE("EPORTE", "导出"),
	/**
	 * 导入
	 */
	IMPORT("IMPORT", "导入"),
	/**
	 * 付款
	 */
	PAY("PAY", "付款");

	private final String code;
	private final String message;

	SystemLogOperateEnums(String code, String message) {
		this.code = code;
		this.message = message;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

	public static String getMessage(String code) {
		for (SystemLogOperateEnums e : SystemLogOperateEnums.values()) {
			if (e.getCode().equals(code)) {
				return e.getMessage();
			}
		}
		return null;
	}

}
