## 代码生成器文档

### 项目介绍

    一键生成后台系统框架代码，包含有MyBatis的Mapper XML文件，数据持久层Dao,业务逻辑层Service,控制层Controller 的代码，自动生成Mybatis Mapper增删改查，分页查询SQL；生成的类继承BaseDao,BaseService,BaseController，BaseEntity，把公共的增删改查，分页查询抽象封装出来，方便不用写重复的简单查询，单体新增和修改，删除方法。


