package org.john.springbootadmin.controller.system.controller;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.john.springbootadmin.controller.system.entity.SysUser;
import org.john.springbootadmin.controller.system.service.SysUserService;
import com.design.framework.base.controller.BaseController;

/**
 * 系统用户管理
 * @author johnDeng
 * @dateTime 2019-03-18 10:20:35
 */
@Controller
@RequestMapping("/pc/sysUser")
public class SysUserController extends BaseController<SysUser,String>{
	
	@Resource
	private SysUserService sysUserService;
	
	@PostConstruct
	public void initialize() {
		super.setBaseService(sysUserService);
	}
	
}

