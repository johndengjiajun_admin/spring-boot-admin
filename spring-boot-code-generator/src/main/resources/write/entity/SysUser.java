package org.john.springbootadmin.controller.system.entity;

import com.design.framework.base.entity.BaseEntity;
import org.apache.ibatis.type.Alias;
import javax.persistence.Table;

/**
 * 系统用户管理
 * @author johnDeng
 * @dateTime 2019-03-18 10:20:35
 */
@Alias(value = "sysUser")
@Table(name="tb_sys_user")
public class SysUser extends BaseEntity<String>{
	
	private static final long serialVersionUID = 1L;
	
/**
*登录名
*/
    private String username;
/**
*真实姓名
*/
    private String name;
/**
*密码
*/
    private String password;
/**
*性别，1男2女
*/
    private String sex;
/**
*编号
*/
    private String staffNo;
/**
*邮箱
*/
    private String email;
/**
*手机号码
*/
    private String mobilePhone;
/**
*是否生效,0是，1否
*/
    private Integer status;
    public void setUsername(String username) {this.username = username;}
    public String getUsername() { return username; }
    public void setName(String name) {this.name = name;}
    public String getName() { return name; }
    public void setPassword(String password) {this.password = password;}
    public String getPassword() { return password; }
    public void setSex(String sex) {this.sex = sex;}
    public String getSex() { return sex; }
    public void setStaffNo(String staffNo) {this.staffNo = staffNo;}
    public String getStaffNo() { return staffNo; }
    public void setEmail(String email) {this.email = email;}
    public String getEmail() { return email; }
    public void setMobilePhone(String mobilePhone) {this.mobilePhone = mobilePhone;}
    public String getMobilePhone() { return mobilePhone; }
    public void setStatus(Integer status) {this.status = status;}
    public Integer getStatus() { return status; }

	

}

