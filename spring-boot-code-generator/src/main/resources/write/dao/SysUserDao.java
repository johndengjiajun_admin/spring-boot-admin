package org.john.springbootadmin.controller.system.dao;

import com.design.framework.base.dao.BaseDao;
import org.john.springbootadmin.controller.system.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统用户管理
 * @author johnDeng
 * @dateTime 2019-03-18 10:20:35
 */
@Mapper
public interface SysUserDao extends BaseDao<SysUser,String>{
	
}

