package org.john.springbootadmin.controller.system.service.impl;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.design.framework.base.service.impl.BaseServiceImpl;
import org.john.springbootadmin.controller.system.dao.SysUserDao;
import org.john.springbootadmin.controller.system.entity.SysUser;
import org.john.springbootadmin.controller.system.service.SysUserService;
/**
 * 系统用户管理
 * @author johnDeng
 * @dateTime 2019-03-18 10:20:35
 */
@Service
public class SysUserServiceImpl extends BaseServiceImpl<SysUser,String> implements SysUserService {
	@Resource
	private SysUserDao sysUserDao;
	
	@PostConstruct
	public void initialize(){
		super.setBaseDao(sysUserDao);
	}
	
}

