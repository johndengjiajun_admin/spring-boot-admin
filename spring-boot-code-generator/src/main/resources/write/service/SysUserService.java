package org.john.springbootadmin.controller.system.service;

import com.design.framework.base.service.BaseService;
import org.john.springbootadmin.controller.system.entity.SysUser;

/**
 * 系统用户管理
 * @author johnDeng
 * @dateTime 2019-03-18 10:20:35
 */
public interface SysUserService extends BaseService<SysUser,String> {


}

