package ${packgePath};

import ${extendsBasePath}.service.BaseService;
import ${importEntityClassPackgePath}.${className};

/**
 * ${classDescription}
 * @author ${author}
 * @dateTime ${datatime}
 */
public interface ${className}Service extends BaseService<${className},${primaryKey}> {


}
