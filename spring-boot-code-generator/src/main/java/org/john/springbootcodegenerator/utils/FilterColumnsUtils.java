package org.john.springbootcodegenerator.utils;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.john.springbootcodegenerator.bean.TableColumns;

public class FilterColumnsUtils {

	/**
	 * 过滤实体字段
	 */
	public static  List<String> arrayEntity = Arrays.asList();
	
	public static  List<String> arrayMapper = Arrays.asList();

	
	public static List<TableColumns> filter(List<TableColumns> list, List<String> array) {
		if(CollectionUtils.isEmpty(array)){
			return list;
		}
		Iterator<TableColumns> it = list.iterator();
		while (it.hasNext()) {
			String name = it.next().getColumnName();
			for (int i = 0; i < array.size(); i++) {
				if (array.get(i).equals(name)) {
					it.remove();
				}
			}
		}
		return list;
	}
	
	
	
	

}
