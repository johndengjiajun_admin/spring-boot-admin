package org.john.springbootcodegenerator.jdbc;

import java.util.List;

import org.john.springbootcodegenerator.bean.TableColumns;
import org.john.springbootcodegenerator.config.CodeGeneratorConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class TableColumnsJdbc {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private CodeGeneratorConfig jdbcTableConfig;

	/**
	 * 获取表结构
	 * 
	 * @author johnDeng
	 * @dateTime 2019年3月5日下午3:51:10
	 * @param tableName 表名
	 * @return List<TableColumns>
	 */
	public List<TableColumns> getListByTable(String tableName) {
		String sql = "SELECT TABLE_SCHEMA AS tableSchema,TABLE_NAME AS tableName,COLUMN_NAME AS columnName,"
				+ " ORDINAL_POSITION AS ordinalPosition,IS_NULLABLE AS notNullFlag,DATA_TYPE AS dataType,"
				+ " CHARACTER_MAXIMUM_LENGTH AS columnLength,COLUMN_KEY AS  cloumnKey,COLUMN_COMMENT AS cloumnComent "
				+ " FROM information_schema.columns WHERE table_schema = ? AND table_name = ?";
		logger.debug(sql);
		return jdbcTemplate.query(sql,new Object[]{jdbcTableConfig.getDatabaseName(),tableName},new BeanPropertyRowMapper<TableColumns>(TableColumns.class));
	}


}
