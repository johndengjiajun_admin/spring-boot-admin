package org.john.springbootcodegenerator.common;

/**
 * 公共类
 * @author JohnDeng
 * @dateTime 2019年1月17日下午5:24:11
 */
public class Commons {

	/**
	 * 测试标识
	 */
	public static final boolean TEST_FLAG=false;
	/**
	 * 主键
	 */
	public static final String PK = "PRI";
	/**
	 * 实体模板名称
	 */
	public static final String TMPLATE_ENTITY_FILE_NAME="entityTemplate.txt";
	/**
	 * DAO模板名称
	 */
	public static final String TMPLATE_DAO_FILE_NAME="daoTemplate.txt";
	/**
	 * service模板名称
	 */
	public static final String TMPLATE_SERVICE_FILE_NAME="serviceTemplate.txt";
	/**
	 * serviceimpl模板名称
	 */
	public static final String TMPLATE_SERVICE_IMPL_FILE_NAME="serviceImplTemplate.txt";
	/**
	 * controller模板名称
	 */
	public static final String TMPLATE_CONTROLLER_FILE_NAME="controllerTemplate.txt";
	/**
	 * mapper模板名称
	 */
	public static final String TMPLATE_MAPPER_FILE_NAME="mapperTemplate.xml";
	
	
	
	
	
}
