package org.john.springbootcodegenerator.service;

public interface TemplateGeneratorService {

	/**
	 * 创建实体模板
	 * @author johnDeng
	 * @dateTime 2019年3月13日下午6:24:38
	 * @param tableName
	 * @param classDescription
	 */
	public void createEntityTemplate(String tableName, String classDescription);
	
	/**
	 * 创建DAO接口模板
	 * @author johnDeng
	 * @dateTime 2019年3月13日下午6:24:54
	 * @param tableName
	 * @param classDescription
	 */
	public void createDaoTemplate(String tableName, String classDescription);
	
	/**
	 * 创建Service接口模板
	 * @author johnDeng
	 * @dateTime 2019年3月13日下午6:25:07
	 * @param tableName
	 * @param classDescription
	 */
	public void createServiceTemplate(String tableName, String classDescription);
	
	/**
	 * 创建Service实现类模板
	 * @author johnDeng
	 * @dateTime 2019年3月13日下午6:25:26
	 * @param tableName
	 * @param classDescription
	 */
	public void createServiceImplTemplate(String tableName, String classDescription) ;
	
	/**
	 * 创建控制器模板
	 * @author johnDeng
	 * @dateTime 2019年3月13日下午6:25:46
	 * @param tableName
	 * @param classDescription
	 */
	public void createControllerTemplate(String tableName, String classDescription);
	
	/**
	 * 创建mybaits-mapper模板
	 * @author johnDeng
	 * @dateTime 2019年3月13日下午6:31:11
	 * @param tableName
	 * @param classDescription
	 */
	public void createMapperTeplate(String tableName, String classDescription);
	
}
