package org.john.springbootcodegenerator.service.impl;

import javax.annotation.Resource;

import org.john.springbootcodegenerator.config.CodeGeneratorConfig;
import org.john.springbootcodegenerator.config.PathConfig;
import org.john.springbootcodegenerator.service.MyBatisMapperService;
import org.john.springbootcodegenerator.service.TableColumnsService;
import org.john.springbootcodegenerator.service.TemplateGeneratorService;
import org.john.springbootcodegenerator.utils.DateTimeUtils;
import org.john.springbootcodegenerator.utils.FileUtlis;
import org.john.springbootcodegenerator.utils.FormatNameUtlis;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
@Service
public class TemplateGeneratorServiceImpl implements TemplateGeneratorService {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Resource
	private TableColumnsService tableColumnsService;
	@Resource
	private CodeGeneratorConfig codeGeneratorConfig;
	@Resource
	MyBatisMapperService myBatisMapperService;
	@Resource
	private PathConfig pathConfig;
	
	
	@Override
	public void createEntityTemplate(String tableName, String classDescription) {
	
		logger.info(">>>>>开始创建实体<<<<<");
		String templateEntityString = FileUtlis.readFileText(pathConfig.getTemplateEntityFileName());
		String entityData = tableColumnsService.getEntityData(tableName);
		templateEntityString = templateEntityString.replace("${classDescription}", classDescription);
		templateEntityString = templateEntityString.replace("${author}", codeGeneratorConfig.getAuthor());
		templateEntityString = templateEntityString.replace("${datatime}", DateTimeUtils.getDatatime());
		templateEntityString = templateEntityString.replace("${alias}", tableColumnsService.getAliasName(tableName));
		templateEntityString = templateEntityString.replace("${table}", tableName);
		templateEntityString = templateEntityString.replace("${className}",tableColumnsService.getClassName(tableName));
		templateEntityString = templateEntityString.replace("${primaryKey}",tableColumnsService.getPrimaryKeyDataType(tableName));
		templateEntityString = templateEntityString.replace("${entityData}", entityData);
		templateEntityString = templateEntityString.replace("${packgePath}", codeGeneratorConfig.getPackgePath("entity"));
		templateEntityString = templateEntityString.replace("${extendsBasePath}", codeGeneratorConfig.getExtendsBasePath());
		logger.info("templateEntityString:" + templateEntityString);
		FileUtlis.wirteContent(pathConfig.getWriteEntityFilePath(),tableColumnsService.getClassName(tableName) + ".java", templateEntityString);
		logger.info(">>>>>结束创建实体<<<<<");
		
	}

	@Override
	public void createDaoTemplate(String tableName, String classDescription) {
		
		logger.info(">>>>>开始创建DAO<<<<<");
		String templateDaoString = FileUtlis.readFileText(pathConfig.getTemplateDaoFileName());
		templateDaoString = templateDaoString.replace("${packgePath}",  codeGeneratorConfig.getPackgePath("dao"));
		templateDaoString = templateDaoString.replace("${importEntityClassPackgePath}",  codeGeneratorConfig.getImportEntityClassPackgePath());
		templateDaoString = templateDaoString.replace("${className}", tableColumnsService.getClassName(tableName));
		templateDaoString = templateDaoString.replace("${classDescription}", classDescription);
		templateDaoString = templateDaoString.replace("${author}", codeGeneratorConfig.getAuthor());
		templateDaoString = templateDaoString.replace("${datatime}", DateTimeUtils.getDatatime());
		templateDaoString = templateDaoString.replace("${primaryKey}",tableColumnsService.getPrimaryKeyDataType(tableName));
		templateDaoString = templateDaoString.replace("${extendsBasePath}", codeGeneratorConfig.getExtendsBasePath());

		logger.info("templateDaoString:" + templateDaoString);
		FileUtlis.wirteContent(pathConfig.getWriteDaoFilePath(),
				tableColumnsService.getClassName(tableName) + "Dao.java", templateDaoString);
		logger.info(">>>>>结束创建DAO<<<<<");
		
	}

	@Override
	public void createServiceTemplate(String tableName, String classDescription) {
		
		logger.info(">>>>>开始创建Service<<<<<");
		String templateServiceString = FileUtlis.readFileText(pathConfig.getTemplateServiceFileName());
		templateServiceString = templateServiceString.replace("${packgePath}",  codeGeneratorConfig.getPackgePath("service"));
		templateServiceString = templateServiceString.replace("${importEntityClassPackgePath}",  codeGeneratorConfig.getImportEntityClassPackgePath());
		templateServiceString = templateServiceString.replace("${className}", tableColumnsService.getClassName(tableName));
		templateServiceString = templateServiceString.replace("${classDescription}", classDescription);
		templateServiceString = templateServiceString.replace("${author}", codeGeneratorConfig.getAuthor());
		templateServiceString = templateServiceString.replace("${datatime}", DateTimeUtils.getDatatime());
		templateServiceString = templateServiceString.replace("${primaryKey}",tableColumnsService.getPrimaryKeyDataType(tableName));
		templateServiceString = templateServiceString.replace("${extendsBasePath}", codeGeneratorConfig.getExtendsBasePath());

		logger.info("templateServiceString:" + templateServiceString);
		FileUtlis.wirteContent(pathConfig.getWriteServiceFilePath(),
				tableColumnsService.getClassName(tableName) + "Service.java", templateServiceString);
		logger.info(">>>>>结束创建Service<<<<<");
	}

	@Override
	public void createServiceImplTemplate(String tableName, String classDescription) {
		
		logger.info(">>>>>开始创建ServiceImpl<<<<<");
		String templateServiceImplString = FileUtlis.readFileText(pathConfig.getTemplateServiceImplFileName());
		templateServiceImplString = templateServiceImplString.replace("${packgePath}", codeGeneratorConfig.getPackgePath("serviceImpl"));
		templateServiceImplString = templateServiceImplString.replace("${importEntityClassPackgePath}",  codeGeneratorConfig.getImportEntityClassPackgePath());
		templateServiceImplString = templateServiceImplString.replace("${importDaoClassPackgePath}",  codeGeneratorConfig.getImportDaoClassPackgePath());
		templateServiceImplString = templateServiceImplString.replace("${importServiceClassPackgePath}",  codeGeneratorConfig.getImportServiceClassPackgePath());

		templateServiceImplString = templateServiceImplString.replace("${className}", tableColumnsService.getClassName(tableName));
		templateServiceImplString = templateServiceImplString.replace("${className1}", FormatNameUtlis.formatNameLowerCase(tableColumnsService.getClassName(tableName)));
		templateServiceImplString = templateServiceImplString.replace("${classDescription}", classDescription);
		templateServiceImplString = templateServiceImplString.replace("${author}", codeGeneratorConfig.getAuthor());
		templateServiceImplString = templateServiceImplString.replace("${datatime}", DateTimeUtils.getDatatime());
		templateServiceImplString = templateServiceImplString.replace("${primaryKey}",tableColumnsService.getPrimaryKeyDataType(tableName));
		templateServiceImplString = templateServiceImplString.replace("${extendsBasePath}", codeGeneratorConfig.getExtendsBasePath());

		logger.info("templateServiceImplString:" + templateServiceImplString);
		FileUtlis.wirteContent(pathConfig.getWriteServiceImplFilePath(),tableColumnsService.getClassName(tableName) + "ServiceImpl.java", templateServiceImplString);
		logger.info(">>>>>结束创建ServiceImpl<<<<<");
		
	}

	@Override
	public void createControllerTemplate(String tableName, String classDescription) {
		logger.info(">>>>>开始创建controller<<<<<");
		String templateControllerString = FileUtlis.readFileText(pathConfig.getTemplateControllerFileName());
		templateControllerString = templateControllerString.replace("${packgePath}",codeGeneratorConfig.getPackgePath("controller"));
		templateControllerString = templateControllerString.replace("${importEntityClassPackgePath}",  codeGeneratorConfig.getImportEntityClassPackgePath());
		templateControllerString = templateControllerString.replace("${importServiceClassPackgePath}",  codeGeneratorConfig.getImportServiceClassPackgePath());
		templateControllerString = templateControllerString.replace("${className}", tableColumnsService.getClassName(tableName));
		templateControllerString = templateControllerString.replace("${className1}", FormatNameUtlis.formatNameLowerCase(tableColumnsService.getClassName(tableName)));
		templateControllerString = templateControllerString.replace("${classDescription}", classDescription);
		templateControllerString = templateControllerString.replace("${author}", codeGeneratorConfig.getAuthor());
		templateControllerString = templateControllerString.replace("${datatime}", DateTimeUtils.getDatatime());
		templateControllerString = templateControllerString.replace("${primaryKey}",tableColumnsService.getPrimaryKeyDataType(tableName));
		templateControllerString = templateControllerString.replace("${extendsBasePath}", codeGeneratorConfig.getExtendsBasePath());

		logger.info("templateControllerString:" + templateControllerString);
		FileUtlis.wirteContent(pathConfig.getWriteControllerFilePath(),tableColumnsService.getClassName(tableName) + "Controller.java", templateControllerString);
		logger.info(">>>>>结束创建controller<<<<<");
		
	}

	@Override
	public void createMapperTeplate(String tableName, String classDescription) {
		logger.info(">>>>>开始创建Mapper<<<<<");
		String templateMapperString = FileUtlis.readFileText(pathConfig.getTemplateMapperFileName());
		templateMapperString=templateMapperString.replace("${tableName}", tableName);
		templateMapperString=templateMapperString.replace("${className}", tableColumnsService.getClassName(tableName));
		templateMapperString=templateMapperString.replace("${mapperNamespace}", codeGeneratorConfig.getPackgePath("dao"));
		templateMapperString=templateMapperString.replace("${alias}", FormatNameUtlis.formatNameLowerCase(tableColumnsService.getClassName(tableName)));
		templateMapperString=templateMapperString.replace("${Columms}", myBatisMapperService.getMapperColumns(tableName));
		templateMapperString=templateMapperString.replace("${insertColumms}", myBatisMapperService.getInsertColums(tableName));
		templateMapperString=templateMapperString.replace("${insertBatchValues}", myBatisMapperService.getInsertBatchValue(tableName));
		templateMapperString=templateMapperString.replace("${insertValues}", myBatisMapperService.getInsertValues(tableName));
		templateMapperString=templateMapperString.replace("${updateColumms}", myBatisMapperService.getUpdateColumms(tableName));
		
		logger.info("templateMapperString:" + templateMapperString);
		FileUtlis.wirteContent(pathConfig.getWriteMapperFilePath(),tableColumnsService.getClassName(tableName) + "Dao.xml", templateMapperString);
		logger.info(">>>>>结束创建Mapper<<<<<");
	}

}
