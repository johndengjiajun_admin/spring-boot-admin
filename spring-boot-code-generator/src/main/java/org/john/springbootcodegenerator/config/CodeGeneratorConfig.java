package org.john.springbootcodegenerator.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 代码生成器配置
 * 
 * @author JohnDeng
 * @dateTime 2019年3月3日下午3:54:46
 */
@Component
public class CodeGeneratorConfig {

	/**
	 * 作者
	 */
	@Value("${my.author}")
	public String author;

	/**
	 * 包路径
	 */
	@Value("${my.packgePath}")
	private String packgePath;

	/**
	 * 生成继承文件总路径
	 */
	@Value("${my.extendsBasePath}")
	private String extendsBasePath;

	/**
	 * 表名
	 */
	@Value("${spring.datasource.url}")
	public String url;

	/**
	 * 数据库表名前缀
	 */
	@Value("${my.tablePrefix}")
	private String tablePrefix;

	@Autowired
	private PathConfig pathConfig;

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPackgePath() {
		return packgePath;
	}

	public void setPackgePath(String packgePath) {
		this.packgePath = packgePath;
	}

	public String getExtendsBasePath() {
		return extendsBasePath;
	}

	public void setExtendsBasePath(String extendsBasePath) {
		this.extendsBasePath = extendsBasePath;
	}

	public String getTablePrefix() {
		return tablePrefix;
	}

	public void setTablePrefix(String tablePrefix) {
		this.tablePrefix = tablePrefix;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDatabaseName() {
		int i = this.url.lastIndexOf("/");
		int j = this.url.indexOf("?");
		return this.url.substring(i + 1, j);
	}

	/**
	 * 获取文件包路径
	 * 
	 * @author johnDeng
	 * @dateTime 2019年3月18日上午11:13:48
	 * @param name
	 * @return
	 */
	public String getPackgePath(String name) {
		String path = "";
		switch (name) {
		case "entity":
			path = this.packgePath + ".entity." + pathConfig.getModuleName();
			break;
		case "dao":
			path = this.packgePath + ".dao." + pathConfig.getModuleName();
			break;
		case "controller":
			path = this.packgePath + ".controller." + pathConfig.getModuleName();
			break;
		case "service":
			path = this.packgePath + ".service." + pathConfig.getModuleName();
			break;
		case "serviceImpl":
			path = this.packgePath + ".service." + pathConfig.getModuleName() + ".impl";
			break;
		default:
			break;
		}
		return path;
	}

	/**
	 * 获取引入实体类包路径
	 * 
	 * @author johnDeng
	 * @dateTime 2019年3月18日上午11:20:47
	 * @return
	 */
	public String getImportEntityClassPackgePath() {

		return this.packgePath + ".entity." + pathConfig.getModuleName();
	}

	public String getImportServiceClassPackgePath() {

		return this.packgePath + ".service." + pathConfig.getModuleName();
	}

	public String getImportDaoClassPackgePath() {

		return this.packgePath + ".dao." + pathConfig.getModuleName();
	}
	
	public String getImportServiceImplClassPackgePath() {

		return this.packgePath + ".service." + pathConfig.getModuleName()+".impl";
	}

}
