package org.john.springbootcodegenerator.config;

import org.apache.commons.lang3.StringUtils;
import org.john.springbootcodegenerator.common.Commons;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 路径配置
 * 
 * @author johnDeng
 * @dateTime 2019年3月13日下午6:35:28
 */
@Component
public class PathConfig {

	/**
	 * 读取模板总路径
	 */
	@Value("${my.templateBasePath}")
	private String templateBasePath;

	/**
	 * 生成文件总路径
	 */
	@Value("${my.writeFileBasePath}")
	private String writeFileBasePath;

	/**
	 * 模块名称
	 */
	@Value("${my.moduleName}")
	private String moduleName;

	/**
	 * mybatis生成的路径
	 */
	@Value("${my.mapperPath}")
	private String mapperPath;

	public String getMapperPath() {
		return mapperPath;
	}

	public void setMapperPath(String mapperPath) {
		this.mapperPath = mapperPath;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getTemplateBasePath() {
		return templateBasePath;
	}

	public void setTemplateBasePath(String templateBasePath) {
		this.templateBasePath = templateBasePath;
	}

	public String getWriteFileBasePath() {
		return writeFileBasePath;
	}

	public void setWriteFileBasePath(String writeFileBasePath) {
		this.writeFileBasePath = writeFileBasePath;
	}

	/**
	 * 获取模板实体文件名和路径
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年1月17日下午5:24:41
	 * @return
	 */
	public String getTemplateEntityFileName() {

		return this.templateBasePath + Commons.TMPLATE_ENTITY_FILE_NAME;
	}

	/**
	 * 获取模板DAO文件名和路径
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年1月17日下午5:24:41
	 * @return
	 */
	public String getTemplateDaoFileName() {

		return this.templateBasePath + Commons.TMPLATE_DAO_FILE_NAME;
	}

	/**
	 * 获取模板Service文件名和路径
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年1月17日下午5:24:41
	 * @return
	 */
	public String getTemplateServiceFileName() {

		return this.templateBasePath + Commons.TMPLATE_SERVICE_FILE_NAME;
	}

	/**
	 * 获取模板ServiceImpl文件名和路径
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年1月17日下午5:24:41
	 * @return
	 */
	public String getTemplateServiceImplFileName() {
		return this.templateBasePath + Commons.TMPLATE_SERVICE_IMPL_FILE_NAME;
	}

	/**
	 * 获取模板Controller文件名和路径
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年1月17日下午5:24:41
	 * @return
	 */
	public String getTemplateControllerFileName() {
		return this.templateBasePath + Commons.TMPLATE_CONTROLLER_FILE_NAME;
	}

	/**
	 * 获取模板mapper文件名和路径
	 * 
	 * @author johnDeng
	 * @dateTime 2019年3月13日下午6:42:47
	 * @return
	 */
	public String getTemplateMapperFileName() {
		return this.templateBasePath + Commons.TMPLATE_MAPPER_FILE_NAME;
	}

	/**
	 * 获取写出实体文件路径
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年1月17日下午5:31:06
	 * @return
	 */
	public String getWriteEntityFilePath() {

		return this.writeFileBasePath + "entity\\" + this.moduleName;
	}

	/**
	 * 获取写出DAO文件路径
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年1月17日下午5:31:06
	 * @return
	 */
	public String getWriteDaoFilePath() {

		return this.writeFileBasePath + "dao\\" + this.moduleName;
	}

	/**
	 * 获取写出Service文件路径
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年1月17日下午5:31:06
	 * @return
	 */
	public String getWriteServiceFilePath() {

		return this.writeFileBasePath + "service\\" + this.moduleName;
	}

	/**
	 * 获取写出ServiceImpl文件路径
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年1月17日下午5:31:06
	 * @return
	 */
	public String getWriteServiceImplFilePath() {
		boolean flag = true;
		if (StringUtils.isNotEmpty(this.moduleName)) {
			flag = false;
		}
		if (flag) {
			return this.writeFileBasePath + "service\\impl";
		} else {
			return this.writeFileBasePath + "service\\" + this.moduleName + "\\impl";
		}
	}

	/**
	 * 获取写出controller文件路径
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年1月17日下午5:31:06
	 * @return
	 */
	public String getWriteControllerFilePath() {

		return this.writeFileBasePath  + "controller\\"+ this.moduleName;
	}

	/**
	 * 获取写出mapper文件路径
	 * 
	 * @author johnDeng
	 * @dateTime 2019年3月14日下午2:41:02
	 * @return
	 */
	public String getWriteMapperFilePath() {
		if (Commons.TEST_FLAG) {
			return this.writeFileBasePath + "mapper";
		} else {
			return this.mapperPath + this.moduleName;
		}
	}

}
