package org.john.springbootcodegenerator;

import java.util.Arrays;

import org.john.springbootcodegenerator.service.TemplateGeneratorService;
import org.john.springbootcodegenerator.utils.FilterColumnsUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootCodeGeneratorApplicationTests {
	@Autowired
	public TemplateGeneratorService codeGenerator;
	/**
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年1月21日下午4:14:55
	 */
	
	
	@Test
	public void codeGeneratorTest(){
		String tableName="tb_order",classDescription="订单管理";
		//过滤实体baseEntity字段
		FilterColumnsUtils.arrayEntity=Arrays.asList("id","create_time","update_time");
		//过滤修改方法更新创建时间
		FilterColumnsUtils.arrayMapper=Arrays.asList("create_time");
		codeGenerator.createEntityTemplate(tableName, classDescription);
		codeGenerator.createDaoTemplate(tableName, classDescription);
		codeGenerator.createServiceTemplate(tableName, classDescription);
		codeGenerator.createServiceImplTemplate(tableName, classDescription);
		codeGenerator.createControllerTemplate(tableName, classDescription);
		codeGenerator.createMapperTeplate(tableName, classDescription);
	}
	
}
