package org.john.springbootadmin;

import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootAdminApplication.class)
public abstract class BaseTest {
	public Logger log = LoggerFactory.getLogger(getClass());
	
	public abstract void test() throws Exception;
}


