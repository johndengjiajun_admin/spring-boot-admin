package org.john.springbootadmin.redis;

import org.john.springbootadmin.BaseTest;
import org.john.springbootadmin.module.entity.system.SysUser;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import com.design.framework.cache.RedisTemplateCache;

public class TestRedis extends BaseTest{

	@Autowired
	private RedisTemplateCache redisTemplate;
	@Autowired
	private RedisTemplate<String, String> stringRedisTemplate ;
	@Test
	@Override
	public void test() throws Exception {
		SysUser u=new SysUser();
		u.setId(1);
		boolean b=redisTemplate.set("k", 1);
		System.out.println(b);
		Integer i=redisTemplate.get("k", Integer.class);
		System.out.println(i);
		
		System.out.println(redisTemplate.delete("k"));
		//System.out.println(CacheFactory.getRedisTemplateCache().delete("k"));
		
	}
}	
