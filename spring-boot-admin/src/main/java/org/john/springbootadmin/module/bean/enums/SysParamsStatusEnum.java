package org.john.springbootadmin.module.bean.enums;

/**
 * 参数枚举
 * @auth JohnDeng
 * @dateTime 2020年6月21日上午12:12:55	
 *
 */
public enum SysParamsStatusEnum {
	/**
	 * Redis缓存
	 */
	REDIS(0, "Redis缓存"), 
	/**
	 * 设备厂家
	 */
	EquipmentManufacturer(1, "设备厂家"), 
	/**
	 * 芯片厂商
	 */
	ChipManufacturer(2, "芯片厂商"), 
	/**
	 * 版本号
	 */
	VersionNumber(3,"版本号"),
	/**
	 * 应用类型
	 */
	ApplicationType(4, "应用类型"), 
	/**
	 * 证书用途
	 */
	CertificateUse(5, "证书用途");

	private final Integer code;
	private final String message;

	private SysParamsStatusEnum(Integer code, String message) {
		this.code = code;
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public static String getMessage(Integer code) {
		for (SysParamsStatusEnum e : SysParamsStatusEnum.values()) {
			if (code.equals(e.getCode()) ) {
				return e.getMessage();
			}
		}
		return "";
	}
}
