package org.john.springbootadmin.module.controller.system;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.john.springbootadmin.module.bean.vo.SysLogExportVo;
import org.john.springbootadmin.module.entity.system.SysLog;
import org.john.springbootadmin.module.service.system.SysLogService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.design.framework.annotion.SystemLog;
import com.design.framework.base.controller.BaseController;
import com.design.framework.bean.Page;
import com.design.framework.enums.SystemLogOperateEnums;
import com.design.framework.enums.SystemLogTypeEnums;
import com.design.framework.poi.ExportUtils;

/**
 * 系统日志管理
 * 
 * @author JohnDeng
 * @dateTime 2019-04-08 04:08:42
 */
@RestController
@RequestMapping("/sysLog")
@SystemLog(modelName = "系统日志管理")
public class SysLogController extends BaseController<SysLog, Integer> {

	@Resource
	private SysLogService sysLogService;

	@PostConstruct
	@Override
	public void initialize() {
		super.setBaseService(sysLogService);

	}

	@Override
	protected void afterGetListByPage(Page<SysLog> page) {
		if (page != null && CollectionUtils.isNotEmpty(page.getResults())) {
			for (SysLog log : page.getResults()) {
				log.setOperateName(SystemLogOperateEnums.getMessage(log.getOperate()));
				log.setTypeName(SystemLogTypeEnums.getMessage(log.getType()));
			}
		}
	}

	/**
	 * 系统日志导出
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年5月21日上午11:20:59
	 * @throws Exception
	 */
	@SystemLog(operate = SystemLogOperateEnums.EPORTE, description = "系统日志导出")
	@GetMapping("/logExport")
	public void logExport(String startTime, String endTime, String userName, String ipAddress) throws Exception {
		String name = "系统日志导出";
		List<SysLogExportVo> list = sysLogService.getSysLogExportList(startTime, endTime, userName, ipAddress);
		if (CollectionUtils.isNotEmpty(list)) {
			for (SysLogExportVo log : list) {
				log.setOperateName(SystemLogOperateEnums.getMessage(log.getOperate()));
				log.setTypeName(SystemLogTypeEnums.getMessage(log.getType()));
			}
		}
		ExportUtils.exportExcel(list, name, name, SysLogExportVo.class, name + ".xls", response);
	}

}
