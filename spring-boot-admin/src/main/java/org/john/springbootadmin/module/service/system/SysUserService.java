package org.john.springbootadmin.module.service.system;

import org.john.springbootadmin.module.entity.system.SysUser;

import com.design.framework.base.service.BaseService;

/**
 * 系统用户管理
 * 
 * @author johnDeng
 * @dateTime 2019-03-18 11:49:44
 */
public interface SysUserService extends BaseService<SysUser, Integer> {

	/**
	 * 登录
	 * 
	 * @author johnDeng
	 * @dateTime 2019年3月19日上午10:13:42
	 * @param username
	 * @param password
	 * @return
	 */
	SysUser login(SysUser user);

	boolean existPhone(String phone);

	SysUser findByPhone(String phone);
}
