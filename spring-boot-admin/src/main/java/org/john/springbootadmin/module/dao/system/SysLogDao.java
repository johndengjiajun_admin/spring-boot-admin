package org.john.springbootadmin.module.dao.system;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.john.springbootadmin.module.bean.vo.SysLogExportVo;
import org.john.springbootadmin.module.entity.system.SysLog;

import com.design.framework.base.dao.BaseDao;

/**
 * 系统日志管理
 * 
 * @author JohnDeng
 * @dateTime 2019-04-08 04:08:42
 */
@Mapper
public interface SysLogDao extends BaseDao<SysLog, Integer> {

	List<SysLogExportVo> getSysLogExportList(@Param("startTime") String startTime, @Param("endTime") String endTime,
			@Param("userName") String userName, @Param("ipAddress") String ipAddress);

}
