package org.john.springbootadmin.module.dao.system;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.john.springbootadmin.module.entity.system.SysUser;
import org.john.springbootadmin.module.entity.system.SysUserRole;

import com.design.framework.base.dao.BaseDao;

/**
 * 系统用户角色管理
 * 
 * @author johnDeng
 * @dateTime 2019-03-18 02:12:20
 */
@Mapper
public interface SysUserRoleDao extends BaseDao<SysUserRole, Integer> {

	/**
	 * 根据用户id批量删除
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年4月7日下午10:11:07
	 * @param list
	 */
	void deleteBatchByUserIds(List<SysUser> list);

}
