package org.john.springbootadmin.module.dao.system;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.john.springbootadmin.module.entity.system.SysRole;
import org.john.springbootadmin.module.entity.system.SysRolePermission;

import com.design.framework.base.dao.BaseDao;

/**
 * 系统角色权限管理
 * 
 * @author johnDeng
 * @dateTime 2019-03-18 02:13:22
 */
@Mapper
public interface SysRolePermissionDao extends BaseDao<SysRolePermission, Integer> {

	/**
	 * 根据角色id批量刪除系统权限
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年3月27日下午2:44:45
	 * @param list
	 */
	public void deleteBatchByRoleId(List<SysRole> list);
}
