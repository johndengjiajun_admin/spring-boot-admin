package org.john.springbootadmin.module.service.system.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.john.springbootadmin.module.dao.system.SysUserRoleDao;
import org.john.springbootadmin.module.entity.system.SysUser;
import org.john.springbootadmin.module.entity.system.SysUserRole;
import org.john.springbootadmin.module.service.system.SysUserRoleService;
import org.springframework.stereotype.Service;

import com.design.framework.base.service.impl.BaseServiceImpl;

/**
 * 系统用户角色管理
 * 
 * @author johnDeng
 * @dateTime 2019-03-18 02:12:20
 */
@Service
public class SysUserRoleServiceImpl extends BaseServiceImpl<SysUserRole, Integer> implements SysUserRoleService {
	@Resource
	private SysUserRoleDao sysUserRoleDao;

	@PostConstruct
	public void initialize() {
		super.setBaseDao(sysUserRoleDao);
	}

	@Override
	public void addSysUserRole(List<Integer> roleIds, Integer userId) {
		SysUserRole sysUserRole = null;
		if (CollectionUtils.isNotEmpty(roleIds) && userId != null) {
			for (Integer roleId : roleIds) {
				sysUserRole = new SysUserRole();
				sysUserRole.setAddParam();
				sysUserRole.setRoleId(roleId);
				sysUserRole.setUserId(userId);
				sysUserRoleDao.insert(sysUserRole);
			}
		}
	}

	@Override
	public void deleteBatchByUserIds(List<SysUser> list) {

		sysUserRoleDao.deleteBatchByUserIds(list);
	}

	@Override
	public List<Integer> getRolesByUserId(Integer userId) {
		List<Integer> roles = new ArrayList<Integer>();
		SysUserRole sysUserRole = new SysUserRole();
		sysUserRole.setUserId(userId);
		List<SysUserRole> list = sysUserRoleDao.getList(sysUserRole);
		for (SysUserRole userRole : list) {
			roles.add(userRole.getRoleId());
		}
		return roles;
	}

}
