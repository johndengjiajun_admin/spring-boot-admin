package org.john.springbootadmin.module.service.system;

import java.util.List;

import org.john.springbootadmin.module.entity.system.SysRole;
import org.john.springbootadmin.module.entity.system.SysRolePermission;

import com.design.framework.base.service.BaseService;

/**
 * 系统角色权限管理
 * 
 * @author johnDeng
 * @dateTime 2019-03-18 02:13:22
 */
public interface SysRolePermissionService extends BaseService<SysRolePermission, Integer> {

	public void addSysRolePermission(List<Integer> permissionIds, Integer roleId);

	public void deleteBatchByRoleId(List<SysRole> list);
}
