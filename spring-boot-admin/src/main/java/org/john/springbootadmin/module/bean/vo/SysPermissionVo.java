package org.john.springbootadmin.module.bean.vo;

import com.design.framework.tree.TreeNode;

/**
 * 系统权限视图模型对象
 * 
 * @author JohnDeng
 * @dateTime 2019年4月7日下午10:09:02
 * @param <ID>
 */
public class SysPermissionVo<ID> extends TreeNode<ID> {

	private String url;

	private String icon;

	private boolean checked;

	private boolean disabled;

	private String permission;

	private int type;

	private int grade;

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
