package org.john.springbootadmin.module.bean.enums;

public enum ApiCodeEnum {

	/**
	 * 用户名或者密码不正确
	 */
	USER_OR_PWD_INCORRECT("1000", "用户名或者密码不正确"), 
	
	/**
	 * 账号已停用,请联系管理员
	 */
	ACCOUT_IS_STOP("1001", "账号已停用,请联系管理员"),
	/**
	 * 查询导出的数据为空
	 */
	EXPORT_DATA_NULL("1002","查询导出的数据为空"), 
	/**
	 * 用户名重复，请重新输入！
	 */
	SYS_USER_NAME_REPEAT("1003","用户名重复，请重新输入！"), 
	SYS_ROLE_NAME_REPEAT("1004","角色名称重复，请重新输入！"), 
	DEPARTMENT_NAME_REPEAT("1005", "部门名称重复,请重新输入"), 
	ORG_BILL_CODE_REPEAT("1006","订单号重复,请重新输入"), 
	ORG_CODE_REPEAT("1007", "机构编码重复，请重新输入"), 
	ID_HAS_CHILDREN("1008","父集存在子集，不能放在子集下面"), 
	ID_HAS_NOT_CHILDREN("1009", "父集和子集不能一样"),
	HTTP_TOKEN_NO_FIND("1010", "请求综合服务平台TOKEN异常"), 
	LOGIN_COUNT_EXCEED("1011","登录次数过多, 当天不能再登录!"), 
	PASSWORD_HAS_TIME_OUT("1012","用户密码已超过180天,请重新修改密码!");

	
	private final String code;
	private final String message;

	private ApiCodeEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}
