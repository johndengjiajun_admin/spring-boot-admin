package org.john.springbootadmin.module.controller.system;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.john.springbootadmin.module.entity.system.SysRolePermission;
import org.john.springbootadmin.module.service.system.SysRolePermissionService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.design.framework.annotion.SystemLog;
import com.design.framework.base.controller.BaseController;

/**
 * 系统角色权限管理
 * 
 * @author johnDeng
 * @dateTime 2019-03-18 02:13:22
 */
@RestController
@RequestMapping("/sysRolePermission")
@SystemLog(modelName = "系统角色权限管理")
public class SysRolePermissionController extends BaseController<SysRolePermission, Integer> {

	@Resource
	private SysRolePermissionService sysRolePermissionService;

	@PostConstruct
	@Override
	public void initialize() {
		super.setBaseService(sysRolePermissionService);
	}

}
