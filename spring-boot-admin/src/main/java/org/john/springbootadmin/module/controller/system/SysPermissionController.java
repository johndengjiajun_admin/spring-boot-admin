package org.john.springbootadmin.module.controller.system;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.john.springbootadmin.module.bean.enums.ApiCodeEnum;
import org.john.springbootadmin.module.entity.system.SysPermission;
import org.john.springbootadmin.module.service.system.SysPermissionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.design.framework.annotion.SystemLog;
import com.design.framework.api.bean.ApiMap;
import com.design.framework.base.controller.BaseController;
import com.design.framework.enums.SystemLogOperateEnums;
import com.design.framework.exception.bean.CustomException;
import com.design.framework.tree.TreeNode;
import com.design.framework.utils.JsonUtils;

/**
 * 系统权限管理
 * 
 * @author johnDeng
 * @dateTime 2019-03-18 02:12:58
 */
@RestController
@RequestMapping("/sysPermission")
@SystemLog(modelName = "系统权限管理")
public class SysPermissionController extends BaseController<SysPermission, Integer> {

	@Resource
	private SysPermissionService sysPermissionService;

	@PostConstruct
	@Override
	public void initialize() {
		super.setBaseService(sysPermissionService);
	}
	
	
	@Override
	protected void beforeDelete(SysPermission entity, Integer id) throws CustomException {
		List<SysPermission> list=sysPermissionService.getChildrenByParentId(entity.getId());
		if(CollectionUtils.isNotEmpty(list)){
			throw new CustomException(ApiCodeEnum.ID_HAS_CHILDREN.getCode(),ApiCodeEnum.ID_HAS_CHILDREN.getMessage());
		}
	}

	/**
	 * 根据用户获取权限树
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年3月20日上午11:58:15
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/getNavTree")
	@SystemLog(operate = SystemLogOperateEnums.SELECT, description = "根据用户获取权限树")
	public ApiMap getNavTree() throws Exception {
		return returnDataSuccess(sysPermissionService.getMenuTree());
	}

	/**
	 * 获取全部树
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年4月7日下午10:04:49
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/getAllTree")
	@SystemLog(operate = SystemLogOperateEnums.SELECT, description = "获取全部树")
	public ApiMap getAllTree() throws Exception {
		List<TreeNode<Integer>> list = sysPermissionService.getAllTree();
		return returnDataSuccess(list);
	}

	/**
	 * 根据角色获取全部权限
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年4月7日下午10:04:57
	 * @param roleId
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/getAllTreeByRoleId/{roleId}")
	@SystemLog(operate = SystemLogOperateEnums.SELECT, description = "根据角色获取权限")
	public ApiMap getAllTreeByRoleId(@PathVariable("roleId") String roleId) throws Exception {
		if (StringUtils.isEmpty(roleId)) {
			return returnParamError();
		}
		return returnDataSuccess(sysPermissionService.getAllTreeByRoleId(Integer.parseInt(roleId)));
	}

	/**
	 * 获取权限树
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年4月7日下午10:05:05
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/getRoleTree")
	@SystemLog(operate = SystemLogOperateEnums.SELECT, description = "根据角色获取权限")
	public ApiMap getRoleTree() throws Exception {
		String [] array = {};
		addMap("list", sysPermissionService.findAll());
		addMap("checkedId", array);
		return returnSucccssMap();
	}

	/**
	 * 编辑,没有id就新增，有id就修改
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年3月25日下午6:28:14
	 * @param entity
	 * @param jsonObject
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/submit")
	@SystemLog(operate = SystemLogOperateEnums.UPDATE, description = "编辑")
	public ApiMap submit(SysPermission entity, @RequestBody JSONObject jsonObject) throws Exception {
		entity = JsonUtils.toBean(jsonObject, entity);
		if (null != entity.getId()) {
			sysPermissionService.update(entity);
		} else {
			entity.setAddParam();
			sysPermissionService.insert(entity);
		}
		return returnSuccess();
	}
	
	
}
