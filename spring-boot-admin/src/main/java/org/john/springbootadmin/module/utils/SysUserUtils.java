package org.john.springbootadmin.module.utils;

import javax.servlet.http.HttpServletRequest;

import com.design.framework.cache.RedisTemplateCache;
import com.design.framework.utils.SpringContextHolderUtils;
import com.design.framework.utils.StringUtils;

/**
 * 用戶工具类
 * 
 * @author johnDeng
 * @dateTime 2019年3月19日上午10:05:16
 */
public class SysUserUtils {

	/**
	 * 获取token
	 * 
	 * @author johnDeng
	 * @dateTime 2019年3月19日上午10:05:10
	 * @param request
	 * @return
	 */
	public static String getToken(HttpServletRequest request) {
		return request.getHeader("Token");
	}

	/**
	 * 获取用户id
	 * 
	 * @author johnDeng
	 * @dateTime 2019年3月19日上午10:05:37
	 * @param request
	 * @return
	 */
	public static String getUserId(HttpServletRequest request) {
		String token = getToken(request);
		if (StringUtils.isEmpty(token)) {
			return "";
		}
		try {
			 RedisTemplateCache redis=SpringContextHolderUtils.getBean(RedisTemplateCache.class);
			 return redis.get(token, String.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
