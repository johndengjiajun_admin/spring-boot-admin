package org.john.springbootadmin.module.entity.system;

import com.design.framework.base.entity.BaseEntity;
import com.design.framework.validata.group.Add;
import com.design.framework.validata.group.Update;

import org.apache.ibatis.type.Alias;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * 系统角色权限管理
 * 
 * @author johnDeng
 * @dateTime 2019-03-18 02:13:22
 */
@Alias(value = "sysRolePermission")
@Table(name = "tb_sys_role_permission")
public class SysRolePermission extends BaseEntity<Integer> {

	private static final long serialVersionUID = 1L;

	/**
	 * 角色ID
	 */
	@NotNull(groups = { Add.class, Update.class }, message = "角色ID不能为空")
	private Integer roleId;
	/**
	 * 权限ID
	 */
	@NotNull(groups = { Add.class, Update.class }, message = "权限ID不能为空")
	private Integer permissionId;

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public void setPermissionId(Integer permissionId) {
		this.permissionId = permissionId;
	}

	public Integer getPermissionId() {
		return permissionId;
	}

}
