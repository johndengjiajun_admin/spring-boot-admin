package org.john.springbootadmin.module.service.system;

import org.john.springbootadmin.module.entity.system.SysRole;

import com.design.framework.base.service.BaseService;

/**
 * 系统角色管理
 * 
 * @author johnDeng
 * @dateTime 2019-03-18 02:09:25
 */
public interface SysRoleService extends BaseService<SysRole, Integer> {

}
