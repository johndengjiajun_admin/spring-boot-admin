package org.john.springbootadmin.module.service.system;

import java.util.List;
import java.util.Map;

import org.john.springbootadmin.module.entity.system.SysPermission;

import com.design.framework.base.service.BaseService;
import com.design.framework.tree.TreeNode;

/**
 * 系统权限管理
 * 
 * @author johnDeng
 * @dateTime 2019-03-18 02:12:58
 */
public interface SysPermissionService extends BaseService<SysPermission, Integer> {

	/**
	 * 根据用户获取菜单树形
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年3月20日上午11:58:36
	 * @return
	 */
	List<TreeNode<Integer>> getMenuTree();

	/**
	 * 根据角色获取权限
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年3月24日下午5:51:11
	 * @return
	 */
	Map<String, Object> getAllTreeByRoleId(Integer roleId);

	List<TreeNode<Integer>> getAllTree();
	
	List<SysPermission> getChildrenByParentId(Integer parentId);


}
