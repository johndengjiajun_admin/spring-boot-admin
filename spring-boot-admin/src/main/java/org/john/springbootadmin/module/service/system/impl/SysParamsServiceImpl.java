package org.john.springbootadmin.module.service.system.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.john.springbootadmin.module.bean.constant.RedisPrdfixKeyConstant;
import org.john.springbootadmin.module.dao.system.SysParamsDao;
import org.john.springbootadmin.module.entity.system.SysParams;
import org.john.springbootadmin.module.service.system.SysParamsService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.design.framework.base.service.impl.BaseServiceImpl;
import com.design.framework.cache.RedisTemplateCache;

/**
 * 系统参数管理
 * 
 * @author JohnDeng
 * @dateTime 2019-04-09 03:54:57
 */
@Service
public class SysParamsServiceImpl extends BaseServiceImpl<SysParams, Integer> implements SysParamsService {


	@Resource
	private SysParamsDao sysParamsDao;

	@Resource
	private RedisTemplate<String, String> redisTemplate;
	
	@Resource
	private RedisTemplateCache redisTemplateCache;
	
	@PostConstruct
	public void initialize() {
		super.setBaseDao(sysParamsDao);
	}

	@Override
	public int insert(SysParams entity) {
		sysParamsDao.insert(entity);
		return resetReids(RedisPrdfixKeyConstant.SYS_PARAMS + entity.getCode(), entity.getValue());
	}

	@Override
	public int update(SysParams entity) {
		sysParamsDao.update(entity);
		return resetReids(RedisPrdfixKeyConstant.SYS_PARAMS + entity.getCode(), entity.getValue());
	}

	@Override
	public int deleteBatchById(List<SysParams> list) {
		if (CollectionUtils.isNotEmpty(list)) {
			for (SysParams sysParams : list) {
				if (redisTemplateCache.exists(RedisPrdfixKeyConstant.SYS_PARAMS + sysParams.getCode())) {
					redisTemplate.delete(RedisPrdfixKeyConstant.SYS_PARAMS + sysParams.getCode());
				}
			}
		}
		return sysParamsDao.deleteBatchById(list);

	}

	/**
	 * 更新缓存
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年4月26日下午2:57:11
	 * @return
	 */
	private int resetReids(String code, String value) {
		redisTemplateCache.set(code, value);
		return 1;
	}

	@Override
	public long getLoginSessionTime() {
		// TODO Auto-generated method stub
		return 0;
	}



}
