package org.john.springbootadmin.module.service.system.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.john.springbootadmin.module.dao.system.SysRolePermissionDao;
import org.john.springbootadmin.module.entity.system.SysRole;
import org.john.springbootadmin.module.entity.system.SysRolePermission;
import org.john.springbootadmin.module.service.system.SysRolePermissionService;
import org.springframework.stereotype.Service;

import com.design.framework.base.service.impl.BaseServiceImpl;

/**
 * 系统角色权限管理
 * 
 * @author johnDeng
 * @dateTime 2019-03-18 02:13:22
 */
@Service
public class SysRolePermissionServiceImpl extends BaseServiceImpl<SysRolePermission, Integer>
		implements SysRolePermissionService {
	@Resource
	private SysRolePermissionDao sysRolePermissionDao;

	@PostConstruct
	public void initialize() {
		super.setBaseDao(sysRolePermissionDao);
	}

	@Override
	public void addSysRolePermission(List<Integer> permissionIds, Integer roleId) {
		SysRolePermission sysRolePermission = null;
		if (CollectionUtils.isNotEmpty(permissionIds)) {
			for (Integer permissionId : permissionIds) {
				sysRolePermission = new SysRolePermission();
				sysRolePermission.setAddParam();
				sysRolePermission.setPermissionId(permissionId);
				sysRolePermission.setRoleId(roleId);
				sysRolePermissionDao.insert(sysRolePermission);
			}
		}

	}

	@Override
	public void deleteBatchByRoleId(List<SysRole> list) {
		sysRolePermissionDao.deleteBatchByRoleId(list);
	}

}
