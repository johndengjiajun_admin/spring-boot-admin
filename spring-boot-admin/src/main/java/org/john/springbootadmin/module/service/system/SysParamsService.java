package org.john.springbootadmin.module.service.system;

import org.john.springbootadmin.module.entity.system.SysParams;

import com.design.framework.base.service.BaseService;

/**
 * 系统参数管理
 * 
 * @author JohnDeng
 * @dateTime 2019-04-09 03:54:56
 */
public interface SysParamsService extends BaseService<SysParams, Integer> {

	long getLoginSessionTime();


}
