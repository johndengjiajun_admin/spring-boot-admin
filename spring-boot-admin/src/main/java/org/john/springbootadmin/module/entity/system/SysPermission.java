package org.john.springbootadmin.module.entity.system;

import com.design.framework.base.entity.BaseEntity;
import javax.persistence.Table;
import org.apache.ibatis.type.Alias;

/**
 * 系统权限管理
 *
 * @author johnDeng
 * @dateTime 2019-03-18 02:12:57
 */
@Alias(value = "sysPermission")
@Table(name = "tb_sys_permission")
public class SysPermission extends BaseEntity<Integer> {

  private static final long serialVersionUID = 1L;

  /**
   * 父模块id
   */
  private Integer parentId;
  /**
   * 模块名称
   */
  private String name;
  /**
   * 链接
   */
  private String url;
  /**
   * 图标地址
   */
  private String imageUrl;
  /**
   * 排序
   */
  private Integer priority;
  /**
   * 权限类型，1功能,2按钮
   */
  private Integer type;
  /**
   * 描述
   */
  private String description;

  /**
   * 等级
   */
  private Integer grade;

  /**
   * 权限标识
   */
  private String permission;

  public String getPermission() {
    return permission;
  }

  public void setPermission(String permission) {
    this.permission = permission;
  }

  public Integer getGrade() {
    return grade;
  }

  public void setGrade(Integer grade) {
    this.grade = grade;
  }

  public void setParentId(Integer parentId) {
    this.parentId = parentId;
  }

  public Integer getParentId() {
    return parentId;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getUrl() {
    return url;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setPriority(Integer priority) {
    this.priority = priority;
  }

  public Integer getPriority() {
    return priority;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public Integer getType() {
    return type;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }

  @Override
  public int hashCode() {
    return this.name.hashCode() * this.getId().hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (this == obj) {
      return true;
    }
    if (obj instanceof SysPermission) {
      SysPermission sysPermission = (SysPermission) obj;
      if (sysPermission.getId().equals(this.getId())) {
        return true;
      }
    }
    return false;
  }

}
