package org.john.springbootadmin.module.dao.system;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.john.springbootadmin.module.entity.system.SysPermission;

import com.design.framework.base.dao.BaseDao;

/**
 * 系统权限管理
 * 
 * @author johnDeng
 * @dateTime 2019-03-18 02:12:57
 */
@Mapper
public interface SysPermissionDao extends BaseDao<SysPermission, Integer> {

	/**
	 * 根据用户权限获取菜单
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年3月20日下午3:39:51
	 * @param userId
	 * @return
	 */
	List<SysPermission> getListMenuByUserId(@Param("userId") Integer userId);

	/**
	 * 根据角色获取权限
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年3月25日下午2:17:52
	 * @param roleId
	 * @return
	 */
	List<SysPermission> getListPermissionByRoleId(@Param("roleId") Integer roleId);
	
	/**
	 * 根据父id查找子集列表
	 * @author JohnDeng
	 * @dateTime 2019年12月9日下午11:41:47	
	 * @param parentId
	 * @return
	 */
	List<SysPermission> getChildrenByParentId(@Param("parentId") Integer parentId);

}
