package org.john.springbootadmin.module.bean.constant;

import com.design.framework.common.RedisKeyPrefixCommon;
/**
 * redis缓存前缀常量
 * @author JohnDeng
 * @dateTime 2020年1月18日下午9:32:49	
 *
 */
public class RedisPrdfixKeyConstant extends RedisKeyPrefixCommon {

	/**
	 * 系统参数
	 */
	public final static String SYS_PARAMS = "tb:sys:params:";
	/**
	 * 登录过期时间
	 */
	public final static String LOGIN_SESSION_TIME = "LOGIN_SESSION_TIME";
	/**
	 * 系统接口过滤列表
	 */
	public final static String SYS_WHITE_LIST="SYS_WHITE_LIST";
}
