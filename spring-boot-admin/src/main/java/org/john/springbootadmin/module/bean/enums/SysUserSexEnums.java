package org.john.springbootadmin.module.bean.enums;

import com.design.framework.enums.IApiCodeMessage;

/**
 * 系统用户性别枚举
 * @author JohnDeng
 * @dateTime 2020年1月28日上午11:28:11	
 *
 */
public enum SysUserSexEnums implements IApiCodeMessage<String, String> {

	/**
	 * 男
	 */
	MAN("1", "男"),
	/**
	 * 女
	 */
	WOMAN("2", "女");

	private final String code;
	private final String message;

	private SysUserSexEnums(String code, String message) {
		this.code = code;
		this.message = message;
	}

	/**
	 * 根据状态码获取信息
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年12月6日下午10:14:25
	 * @param code
	 * @return
	 */
	public static String getMessage(String code) {
		for (SysUserSexEnums e : SysUserSexEnums.values()) {
			if (code.equals(e.getCode())) {
				return e.getMessage();
			}
		}
		return "";
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
