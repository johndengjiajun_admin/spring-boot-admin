package org.john.springbootadmin.module.entity.system;

import com.design.framework.annotion.QueryShow;
import com.design.framework.base.entity.BaseEntity;
import org.apache.ibatis.type.Alias;
import javax.persistence.Table;

/**
 * 系统参数管理
 * 
 * @author JohnDeng
 * @dateTime 2019-04-09 03:54:56
 */
@Alias(value = "sysParams")
@Table(name = "tb_sys_params")
public class SysParams extends BaseEntity<Integer> {

	private static final long serialVersionUID = 1L;

	/**
	 * 系统编号
	 */
	private String code;
	/**
	 * 参数值
	 */
	private String value;
	/**
	 * 参数名称
	 */
	private String name;
	/**
	 * 参数类型 1设备厂家，2芯片厂商，3版本号，4应用类型，5证书用途
	 */
	private Integer type;
	/**
	 * 状态，1启动，0停用
	 */
	private Integer status;
	@QueryShow
	private String statusName;
	@QueryShow
	private String typeName;

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/**
	 * @author JohnDeng 2019年4月9日下午3:57:06
	 * @return the statusName
	 */
	public String getStatusName() {
		return statusName;
	}

	/**
	 * @author JohnDeng 2019年4月9日下午3:57:06
	 * @param statusName
	 *            the statusName to set
	 */
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getType() {
		return type;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

}
