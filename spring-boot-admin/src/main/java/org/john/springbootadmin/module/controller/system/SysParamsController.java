package org.john.springbootadmin.module.controller.system;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.john.springbootadmin.module.bean.enums.SysParamsStatusEnum;
import org.john.springbootadmin.module.entity.system.SysParams;
import org.john.springbootadmin.module.service.system.SysParamsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.design.framework.annotion.SystemLog;
import com.design.framework.base.controller.BaseController;
import com.design.framework.bean.Page;

/**
 * 系统参数管理
 * 
 * @author JohnDeng
 * @dateTime 2019-04-09 03:54:57
 */
@RestController
@RequestMapping("/sysParams")
@SystemLog(modelName = "系统参数管理")
public class SysParamsController extends BaseController<SysParams, Integer> {

	@Resource
	private SysParamsService sysParamsService;

	@PostConstruct
	@Override
	public void initialize() {
		super.setBaseService(sysParamsService);
	}

	@Override
	protected void afterGetListByPage(Page<SysParams> page) {
		if (page != null && CollectionUtils.isNotEmpty(page.getResults())) {
			for (SysParams sysParams : page.getResults()) {
				sysParams.setTypeName(SysParamsStatusEnum.getMessage(sysParams.getType()));
				if (sysParams.getStatus() == 0) {
					sysParams.setStatusName("停用");
				} else if (sysParams.getStatus() == 1) {
					sysParams.setStatusName("启动");
				}
			}
		}
	}

}
