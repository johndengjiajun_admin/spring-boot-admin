package org.john.springbootadmin.module.entity.system;

import com.design.framework.base.entity.BaseEntity;
import com.design.framework.validata.group.Add;
import com.design.framework.validata.group.Update;

import org.apache.ibatis.type.Alias;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * 系统用户角色管理
 * 
 * @author johnDeng
 * @dateTime 2019-03-18 02:12:20
 */
@Alias(value = "sysUserRole")
@Table(name = "tb_sys_user_role")
public class SysUserRole extends BaseEntity<Integer> {

	private static final long serialVersionUID = 1L;

	/**
	 * 用户ID
	 */
	@NotNull(groups = { Add.class, Update.class }, message = "用户ID不能为空")
	private Integer userId;
	/**
	 * 角色ID
	 */
	@NotNull(groups = { Add.class, Update.class }, message = "角色ID不能为空")
	private Integer roleId;

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getRoleId() {
		return roleId;
	}

}
