package org.john.springbootadmin.module.entity.system;

import java.util.Date;
import java.util.List;

import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.apache.ibatis.type.Alias;

import com.design.framework.annotion.QueryShow;
import com.design.framework.base.entity.BaseEntity;
import com.design.framework.validata.group.Add;
import com.design.framework.validata.group.Update;

/**
 * 系统用户管理
 * 
 * @author johnDeng
 * @dateTime 2019-03-18 11:49:44
 */
@Alias(value = "sysUser")
@Table(name = "tb_sys_user")
public class SysUser extends BaseEntity<Integer> {

	private static final long serialVersionUID = 1L;

	/**
	 * 登录名
	 */
	@NotNull(groups = { Add.class, Update.class }, message = "登录名不能为空")
	private String username;
	/**
	 * 真实姓名
	 */
	@NotNull(groups = { Add.class, Update.class }, message = "真实姓名不能为空")
	private String name;
	/**
	 * 密码
	 */
	@NotNull(groups = { Add.class, Update.class }, message = "密码不能为空")
	private String password;
	/**
	 * 性别，1男2女
	 */
	@NotNull(groups = { Add.class, Update.class }, message = "性别不能为空")
	private String sex;
	/**
	 * 编号
	 */
	private String staffNo;
	/**
	 * 邮箱
	 */
	@NotNull(groups = { Add.class, Update.class }, message = "邮箱不能为空")
	private String email;
	/**
	 * 手机号码
	 */
	@NotNull(groups = { Add.class, Update.class }, message = "手机号码不能为空")
	private String mobilePhone;
	/**
	 * 是否生效,0是，1否
	 */
	@NotNull(groups = { Add.class, Update.class }, message = "状态不能为空")
	private Integer status;

	/**
	 * 性别名称
	 */
	@QueryShow
	private String sexName;
	/**
	 * 状态名称
	 */
	@QueryShow
	private String statusName;

	/**
	 * 角色id列表
	 */
	@QueryShow
	@NotNull(groups = { Add.class, Update.class }, message = "角色不能为空")
	private List<Integer> role;
	/**
	 * 修改密码时间
	 */
	private Date passUpdateTime;

	public Date getPassUpdateTime() {
		return passUpdateTime;
	}

	public void setPassUpdateTime(Date passUpdateTime) {
		this.passUpdateTime = passUpdateTime;
	}

	public List<Integer> getRole() {
		return role;
	}

	public void setRole(List<Integer> role) {
		this.role = role;
	}

	public String getSexName() {
		return sexName;
	}

	public void setSexName(String sexName) {
		this.sexName = sexName;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getSex() {
		return sex;
	}

	public void setStaffNo(String staffNo) {
		this.staffNo = staffNo;
	}

	public String getStaffNo() {
		return staffNo;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

}
