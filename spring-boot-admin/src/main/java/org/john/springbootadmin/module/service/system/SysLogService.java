package org.john.springbootadmin.module.service.system;

import java.util.List;

import org.john.springbootadmin.module.bean.vo.SysLogExportVo;
import org.john.springbootadmin.module.entity.system.SysLog;

import com.design.framework.base.service.BaseService;

/**
 * 系统日志管理
 * 
 * @author JohnDeng
 * @dateTime 2019-04-08 04:08:42
 */
public interface SysLogService extends BaseService<SysLog, Integer> {

	public void insert(Integer userId, String className, String methodName, String operate, String type,
			String ipAddress, String modelName, String description);

	/**
	 * 获取导出日志
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年5月21日上午11:24:23
	 * @param startTime
	 * @param endTime
	 * @param ipAddress
	 * @param userName
	 * @return
	 */
	public List<SysLogExportVo> getSysLogExportList(String startTime, String endTime, String userName,
			String ipAddress);
}
