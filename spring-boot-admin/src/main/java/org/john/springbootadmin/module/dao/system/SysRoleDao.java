package org.john.springbootadmin.module.dao.system;

import org.apache.ibatis.annotations.Mapper;
import org.john.springbootadmin.module.entity.system.SysRole;

import com.design.framework.base.dao.BaseDao;

/**
 * 系统角色管理
 * 
 * @author johnDeng
 * @dateTime 2019-03-18 02:09:25
 */
@Mapper
public interface SysRoleDao extends BaseDao<SysRole, Integer> {

}
