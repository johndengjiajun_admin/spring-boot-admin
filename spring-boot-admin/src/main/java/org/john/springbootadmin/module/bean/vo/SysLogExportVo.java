package org.john.springbootadmin.module.bean.vo;

import org.apache.ibatis.type.Alias;

import com.alibaba.fastjson.annotation.JSONField;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * 系统日志导出
 * 
 * @author JohnDeng
 * @dateTime 2019年5月21日上午11:06:23
 */
@Alias(value = "sysLogExportVo")
public class SysLogExportVo {

	@Excel(name = "用户名称", orderNum = "1", width = 30)
	private String userName;
	@Excel(name = "IP地址", orderNum = "2", width = 30)
	private String ipAddress;
	@Excel(name = "模块名称", orderNum = "3", width = 30)
	private String modelName;
	@Excel(name = "操作名称", orderNum = "4", width = 30)
	private String operateName;
	@Excel(name = "标题名称", orderNum = "5", width = 30)
	private String description;
	@Excel(name = "终端", orderNum = "6", width = 30)
	private String typeName;
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	@Excel(name = "创建时间", orderNum = "7", width = 30)
	private String createTime;

	private String type;

	private String operate;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOperate() {
		return operate;
	}

	public void setOperate(String operate) {
		this.operate = operate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getOperateName() {
		return operateName;
	}

	public void setOperateName(String operateName) {
		this.operateName = operateName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

}
