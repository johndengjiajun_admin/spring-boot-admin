package org.john.springbootadmin.module.entity.system;

import javax.persistence.Table;

import org.apache.ibatis.type.Alias;

import com.design.framework.annotion.QueryShow;
import com.design.framework.base.entity.BaseEntity;

/**
 * 系统日志管理
 * 
 * @author JohnDeng
 * @dateTime 2019-04-08 04:08:42
 */
@Alias(value = "sysLog")
@Table(name = "tb_sys_log")
public class SysLog extends BaseEntity<Integer> {

	private static final long serialVersionUID = 1L;

	/**
	 * 系统用户id
	 */
	private Integer userId;
	/**
	 * 类名
	 */
	private String className;
	/**
	 * 方法名
	 */
	private String methodName;
	/**
	 * 操作方式(增删改查）
	 */
	private String operate;
	/**
	 * 操作类型（PC,APP)
	 */
	private String type;
	/**
	 * ip地址
	 */
	private String ipAddress;
	/**
	 * 描述
	 */
	private String description;

	/**
	 * 模块名称
	 */
	private String modelName;

	@QueryShow
	private String userName;
	@QueryShow
	private String operateName;
	@QueryShow
	private String typeName;

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	/**
	 * @author JohnDeng 2019年4月9日下午3:21:10
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @author JohnDeng 2019年4月9日下午3:21:10
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @author JohnDeng 2019年4月9日下午3:21:10
	 * @return the operateName
	 */
	public String getOperateName() {
		return operateName;
	}

	/**
	 * @author JohnDeng 2019年4月9日下午3:21:10
	 * @param operateName
	 *            the operateName to set
	 */
	public void setOperateName(String operateName) {
		this.operateName = operateName;
	}

	/**
	 * @author JohnDeng 2019年4月9日下午3:21:10
	 * @return the typeName
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * @author JohnDeng 2019年4月9日下午3:21:10
	 * @param typeName
	 *            the typeName to set
	 */
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public SysLog() {
		super();

	}

	public SysLog(Integer userId, String className, String methodName, String operate, String type, String ipAddress,
			String description) {
		super();
		this.userId = userId;
		this.className = className;
		this.methodName = methodName;
		this.operate = operate;
		this.type = type;
		this.ipAddress = ipAddress;
		this.description = description;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getClassName() {
		return className;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setOperate(String operate) {
		this.operate = operate;
	}

	public String getOperate() {
		return operate;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

}
