package org.john.springbootadmin.module.entity.system;

import java.util.List;

import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.apache.ibatis.type.Alias;

import com.design.framework.annotion.QueryShow;
import com.design.framework.base.entity.BaseEntity;
import com.design.framework.validata.group.Add;
import com.design.framework.validata.group.Update;

/**
 * 系统角色管理
 * 
 * @author johnDeng
 * @dateTime 2019-03-18 02:09:25
 */
@Alias(value = "sysRole")
@Table(name = "tb_sys_role")
public class SysRole extends BaseEntity<Integer> {

	private static final long serialVersionUID = 1L;

	/**
	 * 角色名称
	 */
	@NotNull(groups = { Add.class, Update.class }, message = "角色名称不能为空")
	private String name;
	/**
	 * 描述
	 */
	private String description;

	/**
	 * 权限id
	 */
	@QueryShow
	private List<Integer> permissionIds;

	public List<Integer> getPermissionIds() {
		return permissionIds;
	}

	public void setPermissionIds(List<Integer> permissionIds) {
		this.permissionIds = permissionIds;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

}
