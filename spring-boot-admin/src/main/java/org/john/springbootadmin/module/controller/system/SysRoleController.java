package org.john.springbootadmin.module.controller.system;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.john.springbootadmin.module.bean.enums.ApiCodeEnum;
import org.john.springbootadmin.module.entity.system.SysRole;
import org.john.springbootadmin.module.service.system.SysRoleService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.design.framework.annotion.SystemLog;
import com.design.framework.base.controller.BaseController;
import com.design.framework.exception.bean.CustomException;

/**
 * 系统角色管理
 * 
 * @author johnDeng
 * @dateTime 2019-03-18 02:09:25
 */
@RestController
@RequestMapping("/sysRole")
@SystemLog(modelName = "系统角色管理")
public class SysRoleController extends BaseController<SysRole, Integer> {

	@Resource
	private SysRoleService sysRoleService;

	@PostConstruct
	@Override
	public void initialize() {
		super.setBaseService(sysRoleService);
	}

	@Override
	protected void beforeAdd(SysRole entity, JSONObject json) throws CustomException {
		SysRole role = new SysRole();
		role.setName(entity.getName());
		role = sysRoleService.get(role);
		if (role != null) {
			throw new CustomException(ApiCodeEnum.SYS_ROLE_NAME_REPEAT.getCode(),
					ApiCodeEnum.SYS_ROLE_NAME_REPEAT.getMessage());
		}
	}
}
