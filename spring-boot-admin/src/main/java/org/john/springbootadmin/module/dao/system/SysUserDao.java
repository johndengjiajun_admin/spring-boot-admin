package org.john.springbootadmin.module.dao.system;

import org.apache.ibatis.annotations.Mapper;
import org.john.springbootadmin.module.entity.system.SysUser;

import com.design.framework.base.dao.BaseDao;

/**
 * 系统用户管理
 * 
 * @author johnDeng
 * @dateTime 2019-03-18 11:49:44
 */
@Mapper
public interface SysUserDao extends BaseDao<SysUser, Integer> {

	/**
	 * 登录
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年4月7日下午10:10:55
	 * @param user
	 * @return
	 */
	SysUser login(SysUser user);

}
