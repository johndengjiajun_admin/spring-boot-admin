package org.john.springbootadmin.module.bean.enums;

/**
 * 系统用户状态枚举
 * @author JohnDeng
 * @dateTime 2020年1月28日上午11:27:53	
 *
 */
public enum SysStatusEnums {

	/**
	 * 启用
	 */
	START(1, "启用"),

	/**
	 * 禁用
	 */
	STOP(0, "禁用");

	private final int code;
	private final String message;

	private SysStatusEnums(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public static String getMessage(int code) {
		for (SysStatusEnums e : SysStatusEnums.values()) {
			if (code == e.getCode()) {
				return e.getMessage();
			}
		}
		return "";
	}
}
