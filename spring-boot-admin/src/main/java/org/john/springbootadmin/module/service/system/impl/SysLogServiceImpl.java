package org.john.springbootadmin.module.service.system.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.john.springbootadmin.module.bean.vo.SysLogExportVo;
import org.john.springbootadmin.module.dao.system.SysLogDao;
import org.john.springbootadmin.module.entity.system.SysLog;
import org.john.springbootadmin.module.service.system.SysLogService;
import org.springframework.stereotype.Service;

import com.design.framework.base.service.impl.BaseServiceImpl;

/**
 * 系统日志管理
 * 
 * @author JohnDeng
 * @dateTime 2019-04-08 04:08:42
 */
@Service
public class SysLogServiceImpl extends BaseServiceImpl<SysLog, Integer> implements SysLogService {
	@Resource
	private SysLogDao sysLogDao;

	@PostConstruct
	public void initialize() {
		super.setBaseDao(sysLogDao);
	}

	@Override
	public void insert(Integer userId, String className, String methodName, String operate, String type,
			String ipAddress, String modelName, String description) {
		SysLog entity = new SysLog(userId, className, methodName, operate, type, ipAddress, description);
		entity.setAddParam();
		entity.setModelName(modelName);
		sysLogDao.insert(entity);
	}

	@Override
	public List<SysLogExportVo> getSysLogExportList(String startTime, String endTime, String userName,
			String ipAddress) {

		return sysLogDao.getSysLogExportList(startTime, endTime, userName, ipAddress);
	}

}
