package org.john.springbootadmin.module.dao.system;

import org.apache.ibatis.annotations.Mapper;
import org.john.springbootadmin.module.entity.system.SysParams;

import com.design.framework.base.dao.BaseDao;

/**
 * 系统参数管理
 * 
 * @author JohnDeng
 * @dateTime 2019-04-09 03:54:56
 */
@Mapper
public interface SysParamsDao extends BaseDao<SysParams, Integer> {

}
