package org.john.springbootadmin.module.controller.system;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.john.springbootadmin.module.entity.system.SysUserRole;
import org.john.springbootadmin.module.service.system.SysUserRoleService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.design.framework.annotion.SystemLog;
import com.design.framework.base.controller.BaseController;

/**
 * 系统用户角色管理
 * 
 * @author johnDeng
 * @dateTime 2019-03-18 02:12:20
 */
@RestController
@RequestMapping("/sysUserRole")
@SystemLog(modelName = "系统用户角色管理")
public class SysUserRoleController extends BaseController<SysUserRole, Integer> {

	@Resource
	private SysUserRoleService sysUserRoleService;

	@PostConstruct
	@Override
	public void initialize() {
		super.setBaseService(sysUserRoleService);
	}

}
