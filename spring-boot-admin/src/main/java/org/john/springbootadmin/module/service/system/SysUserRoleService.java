package org.john.springbootadmin.module.service.system;

import java.util.List;

import org.john.springbootadmin.module.entity.system.SysUser;
import org.john.springbootadmin.module.entity.system.SysUserRole;

import com.design.framework.base.service.BaseService;

/**
 * 系统用户角色管理
 * 
 * @author johnDeng
 * @dateTime 2019-03-18 02:12:20
 */
public interface SysUserRoleService extends BaseService<SysUserRole, Integer> {

	/**
	 * 新增用户角色
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年3月27日下午2:32:02
	 *
	 */
	public void addSysUserRole(List<Integer> roleIds, Integer userId);

	/**
	 * 根据用户删除 系统用户角色表
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年3月27日下午2:48:00
	 * @param list
	 */
	public void deleteBatchByUserIds(List<SysUser> list);

	/**
	 * 根据用户获取角色id列表
	 * 
	 * @author JohnDeng
	 * @dateTime 2019年3月27日下午3:33:05
	 * @param userId
	 * @return
	 */
	public List<Integer> getRolesByUserId(Integer userId);

}
