package org.john.springbootadmin.core.aop;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.john.springbootadmin.module.dao.system.SysLogDao;
import org.john.springbootadmin.module.entity.system.SysLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.design.framework.aop.AbstractSystemLogAop;
import com.design.framework.enums.SystemLogTypeEnums;
import com.design.framework.utils.IpUtils;
import com.design.framework.utils.SpringContextHolderUtils;
import com.design.framework.utils.TokenUtils;

/**
 * 系统日志AOP
 * @author JohnDeng
 * @dateTime 2020年6月21日上午12:12:27	
 *
 */
@Aspect
@Component
public class SystemLogAop extends AbstractSystemLogAop {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private HttpServletRequest request;

	@Override
	public void doService(JoinPoint jp) throws Exception {
		logger.debug("------------------方法执行之后start------------------");
		logger.debug("getMethodDescription:" + getMethodDescription(jp));
		logger.debug("getMethodLogOperate:" + getMethodLogOperate(jp));
		logger.debug("getMethodLogType:" + getMethodSystemLogType(jp));
		String tagerClassName = jp.getThis().getClass().getName();
		int i = tagerClassName.indexOf("$$");
		tagerClassName = tagerClassName.subSequence(0, i).toString();
		Class<?> catClass = Class.forName(tagerClassName);
		logger.debug("getSystemLogModelName:" + getSystemLogModelName(catClass));
		if (getMethodSystemLogType(jp).getCode().equals(SystemLogTypeEnums.PC.getCode())) {
			SysLog sysLog = new SysLog();
			sysLog.setAddParam();
			sysLog.setClassName(tagerClassName);
			sysLog.setDescription(getMethodDescription(jp));
			sysLog.setIpAddress(IpUtils.getIpAddr(request));
			sysLog.setMethodName(jp.getSignature().getName());
			sysLog.setOperate(getMethodLogOperate(jp).getCode());
			sysLog.setType(getMethodSystemLogType(jp).getCode());
			sysLog.setUserId(Integer.valueOf(TokenUtils.getUserId(request)));
			sysLog.setModelName(getSystemLogModelName(catClass));
			SysLogDao sysLogDao = SpringContextHolderUtils.getBean(SysLogDao.class);
			sysLogDao.insert(sysLog);
			logger.debug("------------------sysLogDao insert success------------------");
		}
		logger.debug("------------------方法执行之后end------------------");
	}

}
