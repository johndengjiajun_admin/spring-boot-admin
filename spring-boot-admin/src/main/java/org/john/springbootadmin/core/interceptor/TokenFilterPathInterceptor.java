package org.john.springbootadmin.core.interceptor;

import java.util.Arrays;
import java.util.List;

import com.design.framework.interceptor.TokenInterceptor;

/**
 * 过滤器
 * @author JohnDeng
 * @dateTime 2020年6月21日上午12:11:52	
 *
 */
public class TokenFilterPathInterceptor extends TokenInterceptor {
	
	@Override
	public List<String> getTokenFilteUrlConfig() {
		
		return Arrays.asList(new String []{"/sysUser/login"});
	}

}
