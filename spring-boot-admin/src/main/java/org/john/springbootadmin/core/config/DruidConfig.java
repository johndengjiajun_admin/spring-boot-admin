package org.john.springbootadmin.core.config;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * spring.datasource.druid数据源
 *
 * @author JohnDeng
 * @dateTime 2019年3月3日下午5:41:26
 */
@Configuration
public class DruidConfig {

  /**
   * 配置druid
   *
   * @return
   * @author JohnDeng
   * @dateTime 2019年4月7日下午9:29:52
   */
  @Bean
  public ServletRegistrationBean<StatViewServlet> druidServlet() { // 主要实现WEB监控的配置处理
    ServletRegistrationBean<StatViewServlet> servletRegistrationBean = new ServletRegistrationBean<StatViewServlet>(
        new StatViewServlet(), "/druid/*");
    // 白名单
    servletRegistrationBean.addInitParameter("allow", "127.0.0.1");
    // 黑名单
    servletRegistrationBean.addInitParameter("deny", "192.168.1.200");
    // 用户名
    servletRegistrationBean.addInitParameter("loginUsername", "admin");
    // 密码
    servletRegistrationBean.addInitParameter("loginPassword", "123456");
    // 是否可以重置数据源
    servletRegistrationBean.addInitParameter("resetEnable", "false");
    return servletRegistrationBean;
  }

  /**
   * 过滤器
   *
   * @return
   * @author JohnDeng
   * @dateTime 2019年4月7日下午9:29:37
   */
  @Bean
  public FilterRegistrationBean<WebStatFilter> filterRegistrationBean() {
    FilterRegistrationBean<WebStatFilter> filterRegistrationBean = new FilterRegistrationBean<WebStatFilter>();
    filterRegistrationBean.setFilter(new WebStatFilter());
    // 所有请求进行监控处理
    filterRegistrationBean.addUrlPatterns("/*");
    filterRegistrationBean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
    return filterRegistrationBean;
  }

}
