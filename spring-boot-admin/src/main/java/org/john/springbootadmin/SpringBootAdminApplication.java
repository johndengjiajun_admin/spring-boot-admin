package org.john.springbootadmin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.design.framework.cache.RedisTemplateCache;
import com.design.framework.exception.GlobalExceptionHandler;
import com.design.framework.filter.GlobalFilter;
import com.design.framework.utils.SpringContextHolderUtils;
/**
 * 启动类
 * @author JohnDeng
 * @dateTime 2020年1月28日上午11:18:42	
 *
 */
@SpringBootApplication(scanBasePackages = { "org.john.springbootadmin" }, scanBasePackageClasses = {
		SpringContextHolderUtils.class, GlobalExceptionHandler.class,RedisTemplateCache.class })
@ServletComponentScan(basePackageClasses = { GlobalFilter.class })
@MapperScan(value = "org.john.springbootadmin.module.dao")
public class SpringBootAdminApplication extends SpringBootServletInitializer {
	public static void main(String[] args) {
		SpringApplication.run(SpringBootAdminApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(SpringBootAdminApplication.class);
	}

}